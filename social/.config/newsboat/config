# source settings

urls-source "miniflux"
miniflux-url "https://rss.martyoeh.me/"
miniflux-tokeneval "pass show personal/rss.martyoeh.me/Marty | grep newsboat | cut -f2 -d:"

# feed settings

#auto-reload yes
reload-time 120
reload-threads 12
download-retries 4
download-timeout 10
prepopulate-query-feeds yes
# download but hide ignored
ignore-mode display

# display settings

text-width 120

show-read-feeds no
feed-sort-order unreadarticlecount-asc

color listnormal cyan default
color listfocus black default standout bold
color listnormal_unread blue default
color listfocus_unread yellow default bold
color info red black bold
color article white default bold

highlight article "(^Feed:.*|^Title:.*|^Author:.*)" cyan default bold
highlight article "(^Link:.*|^Date:.*)" default default
highlight article "https?://[^ ]+" green default
highlight article "^(Title):.*$" blue default
highlight article "\\[[0-9][0-9]*\\]" magenta default bold
highlight article "\\[image\\ [0-9]+\\]" green default bold
highlight article "\\[embedded flash: [0-9][0-9]*\\]" green default bold
highlight article ":.*\\(link\\)$" cyan default
highlight article ":.*\\(image\\)$" blue default
highlight article ":.*\\(embedded flash\\)$" magenta default

feedlist-format "%?T?║%n %12u %t &╠ %t?"
highlight feedlist "[║│]" color3 color0
highlight feedlist "╠.*" color3 color0

# navigation

goto-next-feed no

bind-key j down feedlist
bind-key k up feedlist
bind-key j next articlelist
bind-key k prev articlelist
bind-key J next-feed articlelist
bind-key K prev-feed articlelist
bind-key j down article
bind-key k up article
bind-key j down searchresultslist
bind-key k up searchresultslist
bind-key l open

# move back everywhere with h, except for quitting app from feedview
bind-key h quit articlelist
bind-key h quit article
bind-key h quit dirbrowser
bind-key h quit filebrowser
bind-key h quit help
bind-key h quit tagselection
bind-key h quit filterselection
bind-key h quit urlview
bind-key h quit searchresultslist

bind-key g home
bind-key G end
bind-key ^F pagedown
bind-key ^U pageup
bind-key ^B halfpageup
bind-key ^D halfpagedown
bind-key n next-unread
bind-key N prev-unread
bind-key ^n next-unread-feed articlelist
bind-key ^p prev-unread-feed articlelist

bind-key s sort
bind-key S rev-sort
bind-key U show-urls

bind-key a toggle-article-read
bind-key f goto-url
bind-key z toggle-show-read-feeds

unbind-key C
unbind-key ^D

macro m set browser umpv ; open-in-browser ; set browser $BROWSER
macro M set browser umpv ; open-in-browser-and-mark-read ; set browser $BROWSER
macro d set browser vidl ; open-in-browser ; set browser $BROWSER
macro d set browser vidl ; open-in-browser-and-mark-read ; set browser $BROWSER
