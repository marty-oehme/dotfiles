# Removable disk configuration

This simple unit depends on `udiskie2` being installed and enables auto-mounting of any inserted removable media.
It will additionally *ignore* any snaps managed by `snapd`, since they would otherwise be detected as loopback devices.

Automatically installs a `systemd` service which is also enabled to start up on system start (as a user service).

The end result is `udiskie` running in the background and automatically mounting any inserted media, 
which will then also show its tray icon so that it can be quickly unmounted as well.
