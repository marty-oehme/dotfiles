#!/usr/bin/env sh
# Updates your system using topgrade if it exists,
# falls back to paru or yay.
# If only pacman exists, will use that but whine about it

# Also makes yay call paru since that is the new hotness
# (or at least I want to try it)

exist paru && {
    # recreate the normal look of yay
    alias yay="paru --bottomup"
}

syu() {
    exist topgrade && {
        topgrade
        return
    }

    exist paru && {
        paru
        return
    }

    exist yay && {
        yay
        return
    }

    exist pacman && {
        echo "Did not find paru, or yay installed. Updates will not be applied to aur packages."
        sudo pacman -Syu
        return
    }
}
