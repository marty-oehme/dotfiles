#!/bin/sh
# more usage instructions at https://github.com/clvv/fasd
# eval "$(fasd --init posix-hook posix_alias bash-hook zsh-hook zsh-ccomp zsh-ccomp-install zsh-wcomp zsh-wcomp-install)"
exist fasd && eval "$(fasd --init auto)"

# any
alias a='fasd -a'
# show / search / select
alias s='fasd -si'
# alias d='fasd -d'        # directory
# alias f='fasd -f'        # file
# alias sd='fasd -sid'     # interactive directory selection
# alias sf='fasd -sif'     # interactive file selection
# cd, same functionality as j in autojump
alias z='fasd_cd -d'
# cd with interactive selection
alias zz='fasd_cd -d -i'

# from: https://github.com/clvv/fasd/issues/10
# since I can only load auto configuration and have default fasd_cd AND useless aliases
# or manually load the modules and NOT have fasd_cd
# it's easier to use this function
fasd_cd() {
    fasd_ret="$(fasd -d "$@")"
    if [ -d "$fasd_ret" ]; then
        cd "$fasd_ret" || exit
    else
        printf "%s\n" "$fasd_ret"
    fi
    unset fasd_ret
}
