#!/usr/bin/env sh
## Quickly bring up a file server for
#  the current directory.

if exist sfz; then
	alias serve="sfz"
elif exist gossa; then
	alias serve="gossa"
elif exist rossa; then
	alias serve="rossa"
fi
