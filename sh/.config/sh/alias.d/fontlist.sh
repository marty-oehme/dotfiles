#!/usr/bin/env sh
#
# list each individual font family installed
# without doing all the duplicate doo-hop of fc-list

# alias fonts='fc-list -f ''%{family}\n'' | awk ''!x[$0]++'''

fontfamilies() {
    fc-list -f '%{family}\n' | awk '!x[$0]++'
}
