#!/usr/bin/env sh
# Add simple 'image ls' for a directory using timg
#
# Prints a thumbnail gallery right in the terminal.
# WILL stutter when you go crazy in a hundred/thousand
# picture folder.

if exist timg; then
    il() {
        images=$(find . -maxdepth 1 -type f -exec file --mime-type {} \+ | awk -F: '{if ($2 ~/image\//) print $1}')
        if [ -z "$images" ]; then {
            echo no images found.
            return
        }; fi
        echo "$images" | timg --grid=4x3 --upscale=i --center --title --frames=1 -f -
    }
    IL() {
        images=$(find . -type f -exec file --mime-type {} \+ | awk -F: '{if ($2 ~/image\//) print $1}')
        if [ -z "$images" ]; then {
            echo no images found.
            return
        }; fi
        echo "$images" | timg --grid=4x3 --upscale=i --center --title --frames=1 -f -
    }
    qr() {
        # if we are in a pipe, read from stdin and set fct arguments to it
        if [ ! -t 0 ]; then
            set -- "$(cat /dev/stdin)"
        fi
        qrencode -s1 -m2 "$@" -o- | timg --upscale=i -
    }
fi
