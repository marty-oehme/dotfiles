#!/usr/bin/env sh

# simple check for running graphical session
if [ -n "$DISPLAY" ] || [ -z "$XDG_VTNR" ] || [ "$XDG_VTNR" -ne 1 ]; then
	return
fi

# more involved check for running graphical session
# but figures it out for weird sshd sessions as well
if [ "$(loginctl show-session "$(loginctl show-user "$(whoami)" -p Display --value)" -p Type --value)" = 'wayland' ]; then
	return
fi

# just in case we are running x11 and this wants
# to automatically start wayland, stop it from doing so
if [ "$(loginctl show-session "$(loginctl show-user "$(whoami)" -p Display --value)" -p Type --value)" = 'x11' ]; then
	return
fi

if command -v river >/dev/null 2>&1; then
	river
fi
