# sh

The bare minimum terminal configuration for a working system.
Contains:

* an XDG compliant home directory setup
* several basic environment variables
* simple aliases
* an optional fzf default setup
* X autostart

While other modules are largely optional, 
this module is the only one strictly necessary for the system to really work at all.

Additionally contains two scripts on which some other modules build:

* a simple script to detect if applications exist
  (and optionally warn the user if they don't)
* and a script to check if internet connectivity exists

