# Wayland desktop environment

Contains:
[riverwm](https://github.com/riverwm/river)
[waybar](https://github.com/Alexays/Waybar)
mako -- notification daemon
font settings
kanshi -- display output setup

The thoughts below are overhauled and fairly old - they are still based on my first days and weeks on wayland.
Takeaway is this: I love the river wm, waybar is unexciting but does what it's supposed to and the wayland environment is totally worth it.

---

My first foray into wayland is based on river,
a tiling window manager somewhat based on bspwm.

This is only a very work-in-progress README file.

Since wayland handles key presses and so on completely differently
from X, 
I can't for example use sxhkd anymore which is a shame.

On the other hand, there is an amazing key *re* binding tool 
(which also works under X I've now found out)
`keyd` which takes care of some X functionality (xcape) at a lower level.

I have not found a good replacement for `clutter` 
(which automatically hides your mouse cursor after inactivity)
which is independent from the window manager.
I believe `swaywm` would include similar functionality, but `river` does not.


## River

River is set up to come close to my old i3 setup. 
Of course, some mappings are different
(especially those for movement between windows),
but overall the keys map to the old ones.

Since the window manager now also takes over the task of compositor
and does not pass through all keys to all programs, 
it takes over the role of `sxhkd` as well and summons other programs.

I am not entirely sure how I feel about this bundling of tasks into one application,
but so far it works.
Since river is also, mimicking bspwm, using an executable file
(any executable file) 
as its configuration file it is also reasonable that the setup can be tamed and refactored better than a single i3 configuration file.

## Waybar

Waybar replaces the old [polybar](https://gitlab.com/marty-oehme/dotfiles/-/tree/89d1402b3e711c4aa473386e47e84f3593e5ae56/polybar) setup.

It displays the first 10 tags on its left, 
with tags highlighted that are either occupied by windows or currently in focus.
In the center it displays a clock which can be clicked to open a simple calendar.

![Simple waybar configuration](.assets/waybar/simple.png)

To the right is where most of the info modules are:
If there are upcoming events listed for the khal application, it will display a calendar module here.
If music is playing through an mpris-compatible player, its status is shown here.
If a connection through wireguard or over a vpn tunnel is established, it is shown here.
Then, from left to right, audio, brightness, wifi, cpu, ram, temperature, and battery information are displayed.

Some displays have alternative display states, with for example the battery showing remaining time and ram information showing used and total available.

Clicking on:

* audio opens pulsemixer
* network opens nmtui
* cpu opens top (or glances on right-click) 
* temperature shows sensors

## keyd

keyd is set up within `/etc/` and not in the dotfiles themselves.
If using the included `./install.sh` file, there is an option to also set up files outside the home directory, including keyd options.
It is configured through `/etc/keyd/default.cfg`.
Currently, it takes care of mapping `capslock` to both control and escape (depending on if its used alone or with other keys),
as well as adding some German characters that I am otherwise missing on my en_US keyboard.
Lastly, it allows easy clipboard pasting with the `insert` key.

## Swaybg

`swaybg` is used to set the wallpaper from the river configuration file.

## Missing

things not yet set up:

* [-] modes: media, academia (worth?)
* [ ] display current desktop mode in status bar
