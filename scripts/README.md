# Scripts module

The scripts module contains several small shell scripts which did not fit under any specific module.
Many of these modules are older and either 'legacy' in that they work but should be restructured at some point,
or perhaps not working at all anymore. It's a bit of a messy module.

Some will work for others without needing changes, some are very personalized to me.

## archive

An 'archiving' script, but not intended for backups. It will instead hardlink all files you give it in a single 'archive' directory, so even if you delete the original file from your directories, the hardlinked copy will remain.

I use it to build a small archive of (mostly youtube videos) that I can delete from my normal folders but always quickly search through and grab again whenever I need them. Will probably not be too useful for most other purposes.

## benchmark

`benchmark` allows you to take the time another commands needs to run.
It will simply output the time taken, and can be run repeatedly or averaged to get more accurate readings.

## bemenu-translate

`bemenu-translate` is a pretty useful tool if you are often writing in a non-native language, doing translation work or similar. In effect, it's a simple wrapper around `trans`, allowing you to use bemenu to input what you want translated and showing the output through the menu as well.

Very useful to have as a little dropdown which you can call via global shortcut.

## lockscreen

`lockscreen` does just that, invoking i3lock/waylock program with some simple defaults and a color, as well as stopping any running media and muting the audio.

## nomie

`nomie` logs a simple journal entry in the open source habit-tracking app [nomie](https://nomie.app).
To do this it needs an api key, which is, as of now, hard-coded to be sourced from my pass app.

## powermenu

A menu to invoke power-related commands: Shut down your PC, reboot, log out, sleep and suspend. Very simple, very useful.

## sharefile

This one could be useful to many: Quickly upload any file to `http://0x0.st` and get the link on your clipboard.
Can take the file to upload through stdin, as an argument, or be chosen interactively with fzf.

## uoeia

Helps you open images, especially remote ones, in your favorite image viewer. Point it at a link and it will open many different kinds of pictures, galleries, and so on.

## vidl

Helps you download videos (mostly from youtube) and archive them using the archive script above.
I use it to download interesting looking videos into an inbox for later viewing.

## wallcrop.sh

Probably not too useful for most: Takes an image that is exactly 3840x1080 pixels and cuts it in half,
naming the resulting images `xy_l.png` and `xy_r.png`. I use it to quickly cut multi-monitor wallpapers to a good display size.

## wallr

Allows you to read articles from your wallabag instance in the terminal. Pretty simple wrapper for the wallabag-cli python library (which is a requirement). Lists your articles using fzf by default and can mark them read if you wish.
