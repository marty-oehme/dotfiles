#!/usr/bin/env bash
# A simple script to make it easy for me to crop newly found wallpapers
# to size, for one 1920x1080 screen on the left and one on the right.
# The resulting images can be used with wallpaper programs (e.g. swww)
# to nicely display per monitor without stretching.
#
# Usage: wallcrop.sh path/to/my/imagefile.jpg
# It will drop two cut versions in the same directory and tell you where
# they are.

# TODO check image dimensions and warn user if not 3840x1080, or at least if smaller than cropped area

main() {
    fullname="$1"
    filename=$(basename -- "$fullname")
    pathonly=${fullname/$filename/}
    extension="${filename##*.}"
    filename="${filename%.*}"

    out_left="${pathonly}${filename}_l.$extension"
    out_right="${pathonly}${filename}_r.$extension"

    printf "input: %s\noutput left:%s\noutput right: %s\n" "$fullname" "$out_left" "$out_right"
    gm convert "$fullname" -crop 1920x1080+0+0 "$out_left"
    gm convert "$fullname" -crop 1920x1080+1920+0 "$out_right"
}

usage="$(basename "$0") [-h] path/to/my/imagefile.jpg -- Simple image cropper for dual screen 1920x1080 setups.

A simple script to make it easy for me to crop newly found wallpapers
to size, for one 1920x1080 screen on the left and one on the right.
The resulting images can be used with wallpaper programs (e.g. swww)
to nicely display per monitor without stretching.

    -h  show this help text

It will drop two cut versions in the same directory and tell you where
they are."

while getopts ':h' option; do
    case "$option" in
    h)
        echo "$usage"
        exit
        ;;
    :)
        printf "missing argument for -%s\n" "$OPTARG" >&2
        echo "$usage" >&2
        exit 1
        ;;
    \?)
        printf "illegal option: -%s\n" "$OPTARG" >&2
        echo "$usage" >&2
        exit 1
        ;;
    esac
done
shift $((OPTIND - 1))

if [ "$#" -ne 1 ]; then
    echo "$usage" >&2
    exit 1
fi
main "$1"
