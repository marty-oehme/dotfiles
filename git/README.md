# Git module

[git](https://git-scm.com/) - a distributed version control system

## What's in this module

[[_TOC_]]

## Global git settings

This is probably the first thing that needs to be customized, since it points to a different identity for each git user.
I sign all my commits by default, so take out the corresponding lines if you don't, or exchange it with your gpg key.

Git will rewrite any remotes using http(s) to use the ssh notation for pushes to github and gitlab so that, even if you set up the repository using an https url you can utilize your usual ssh key for pushing.

Finally, the configuration makes use of a custom pager called `dsf` which is also contained in this module.
It tries to use `diff-so-fancy` if that is installed on the path, otherwise uses git's internal diff prettifier.
If nothing exists it falls back to the standard output.
You can move between changed files in diffs with n/N.

Otherwise, the git config is prepared to handle lfs repositories, and it has an assortment of smaller quality of life changes, though nothing major.

## Basic git command aliases

This module contains a heap of aliases to every-day git commands.
Most of them follow a two-to-three letter combination of things to do which corresponds to the mnemonic of the longer git command.
As such, they are mostly similar to those found in the Oh My Zsh [git plugin](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/git).
Examples of some common aliases are `alias ga=git add`, `alias gst=git status`, `alias gp=git push`, `alias gl=git pull`.

Two aliases might be of note:
`gi` allows quickly generating a gitignore file for the current directory, using the included `gitignore` script.
And `gll` quickly pulls up the contents of the last commit. (`ll` since `gl` already pulls from the remote)

## Gitignore generation script

Adds a `gitignore` script which pulls relevant .gitignore lines from [gitignore.io](https://www.gitignore.io) and sends them to standard out, or creates a .gitignore file in the current directory.

[![asciicast](https://asciinema.org/a/298616.svg)](https://asciinema.org/a/298616)

To show usage of the script run `gitignore -h`. It is fully equipped with zsh auto completions, which will pull a list of all available ignore modules from the website to complete with.

It can alternatively be run through git itself by invoking `git ignore`, which will always generate a .gitignore file in the current directory.

When fzf is installed invoke the script without any arguments to generate a fzf-searchable list of git definitions to create. Select multiple definitions with <tab>.
