#!/usr/bin/env sh

if ! exist git; then
    return 1
fi

# print git version output and get raw version number by stripping prefix
git_version=$(git --version 2>/dev/null)
git_version="${git_version##git version }"

alias g='git'

alias ga='git add'
alias gaa='git add --all'
alias gai='git add -i'
alias gb='git branch'
alias gbd='git branch -d'

alias gc='git commit -v'
alias gc!='git commit -v --amend'
alias gcn!='git commit -v --no-edit --amend'

if version_at_least 2.23 "$git_version"; then
    alias gcm='git switch master 2>/dev/null || git switch main'
    alias gcd='git switch develop 2>/dev/null || git switch staging'
    alias gcb='git switch -c'
else
    alias gcm='git checkout master 2>/dev/null || git checkout main'
    alias gcd='git checkout develop'
    alias gcb='git checkout -b'
fi
alias gco='git checkout'

alias gd='git diff'
alias gds='git diff --staged'

alias gi='git ignore'

# show last committed content
alias gll='git last'
# show quick log overview
alias glg='git log --oneline --decorate --graph'
alias glga='git log --oneline --decorate --graph --remotes --all'
# show detailed log overview
alias glog='git log --stat'
# show detailed log overview with contents
alias gloog='git log --stat -p'

alias gf='git fetch'
alias gfa='git fetchall'
alias gl='git pull'

alias gpn='git push --dry-run'
alias gp='git push'
alias gpf!='git push --force'
alias gpm='git pushmerge'
alias gpa='git pushall'

alias grv='git remote -v'

alias grs='git restore --staged'
alias grs!='git restore'

alias grb='git rebase'
alias grbi='git rebase -i'
alias grbc='git rebase --continue'
alias grbm='git rebase master || git rebase main'

alias gst='git status'

alias gstp='git stash pop'
alias gstl='git stash list'
alias gstL='git stash list --stat'
if version_at_least 2.13 "$git_version"; then
    alias gsta='git stash push'
else
    alias gsta='git stash save'
fi

unset -v git_version
