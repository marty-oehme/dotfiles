-- If the laptop is on battery, the profile 'lq' will be loaded; otherwise 'hq' is used
local mp = require("mp")

local SHOULD_ADJUST = false

local lqprofile = "lowquality"
local hqprofile = "highquality"

local function powerstate()
	local f = io.open("/sys/class/power_supply/AC/online")
	if f == nil then
		return
	end
	local t = f:read("*n")
	f:close()
	return t
end

local function adjust()
	if not SHOULD_ADJUST then
		return
	end

	local state = powerstate()
	-- this actually overrides automatically applied profiles
	-- like 'protocol.http'
	if state == 0 then
		mp.set_property("profile", lqprofile)
		mp.msg.info("[quality] running battery, setting low-quality options.")
		mp.osd_message("[quality] LQ")
	else
		mp.set_property("profile", hqprofile)
		mp.msg.info("[quality] running ac, setting high-quality options.")
		mp.osd_message("[quality] HQ")
	end
end
mp.add_hook("on_load", 1, adjust)
