-- sponsorblock_minimal.lua
--
-- This script skips sponsored segments of YouTube videos
-- using data from https://github.com/ajayyy/SponsorBlock
--
-- original from https://codeberg.org/jouni/mpv_sponsorblock_minimal
-- adapted for local playback skipping and some refactoring by me
local mp = require("mp")

local options = {
	API = "https://sponsor.ajay.app/api/skipSegments",

	-- Categories to fetch and skip
	categories = '"sponsor","intro","outro","interaction","selfpromo"',
}

local function getranges()
	local args = {
		"curl",
		"-s",
		"-d",
		"videoID=" .. Youtube_id,
		"-d",
		"categories=[" .. options.categories .. "]",
		"-G",
		options.API,
	}
	local sponsors = mp.command_native({
		name = "subprocess",
		capture_stdout = true,
		playback_only = false,
		args = args,
	})

	if string.match(sponsors.stdout, "%[(.-)%]") then
		Ranges = {}
		for i in string.gmatch(string.sub(sponsors.stdout, 2, -2), "%[(.-)%]") do
			local k, v = string.match(i, "(%d+.?%d*),(%d+.?%d*)")
			Ranges[k] = v
		end
	end
end

local function skip_ads(_, pos)
	if pos ~= nil then
		for k, v in pairs(Ranges) do
			if tonumber(k) <= pos and tonumber(v) > pos then
				-- this message may sometimes be wrong
				-- it only seems to be a visual thing though
				mp.osd_message(
					"[sponsorblock] skipping forward " .. math.floor(tonumber(v) - mp.get_property("time-pos")) .. "s"
				)
				-- need to do the +0.01 otherwise mpv will start spamming skip sometimes
				-- example: https://www.youtube.com/watch?v=4ypMJzeNooo
				mp.set_property("time-pos", tonumber(v) + 0.01)
				return
			end
		end
	end
end

local function file_loaded()
	local video_path = mp.get_property("path")
	local youtube_id1 = string.match(video_path, "https?://youtu%.be/([%w-_]+).*")
	local youtube_id2 = string.match(video_path, "https?://w?w?w?%.?youtube%.com/v/([%w-_]+).*")
	local youtube_id3 = string.match(video_path, "/watch.*[?&]v=([%w-_]+).*")
	local youtube_id4 = string.match(video_path, "/embed/([%w-_]+).*")
	local localytfile = string.match(video_path, "-([%a%d%-_]+)%.[mw][kpe][v4b][m]?$")
	Youtube_id = youtube_id1 or youtube_id2 or youtube_id3 or youtube_id4 or localytfile
	if not Youtube_id or string.len(Youtube_id) < 11 then
		return
	end
	Youtube_id = string.sub(Youtube_id, 1, 11)

	getranges()
	if Ranges then
		ON = true
		mp.add_key_binding("b", "sponsorblock", toggle)
		mp.observe_property("time-pos", "native", skip_ads)
	end
	return
end

local function toggle()
	if ON then
		mp.unobserve_property(skip_ads)
		mp.osd_message("[sponsorblock] off")
		ON = false
		return
	end
	mp.observe_property("time-pos", "native", skip_ads)
	mp.osd_message("[sponsorblock] on")
	ON = true
	return
end

mp.register_event("file-loaded", file_loaded)
