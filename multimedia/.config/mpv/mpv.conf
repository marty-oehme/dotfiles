## mostly cobbled together from:
## https://github.com/Tsubajashi/mpv-settings/blob/master/mpv.conf
## https://github.com/haasn/gentoo-conf/blob/d3fd304fd7d8d816de6e280daf626b1b49a69893/home/nand/.mpv/config
## with much gratitude

#### player settings

# do not open in fullscreen, do not create border
fullscreen=no
# if playing the last file in e.g. a playlist, simply pause after finishing
keep-open=yes
# allow remote operation through ipc, see https://mpv.io/manual/stable/#json-ipc
input-ipc-server=/tmp/mpv-socket

# display a little seek bar in term
term-osd-bar=yes
# colorful term output
msg-color=yes
# add module names to term output
msg-module=yes

### screenshots

screenshot-directory="${XDG_PICTURES_DIR:-~/media/pictures}/screenshots"
# filename_HH-MM-SS
screenshot-template="screen_%F_%wH-%wM-%wS"
screenshot-format=png
# default compression rate (0 is no compression)
screenshot-png-compression=7
screenshot-tag-colorspace=yes

### OSD

# 2 options necessary for uosc, see https://github.com/darsain/uosc
osc=no
osd-bar=no

osd-font='Iosevka Nerd Font'
osd-font-size=15

### Subtitles

## options and compatibility
# vsfilter backward compatibility
sub-ass-vsfilter-blur-compat=yes
sub-ass-scale-with-window=no
# custom ass style params
sub-ass-force-style=Kerning=yes
# fuzzy-search available subs in folder
sub-auto=fuzzy
# search for external subs in these relative subdirectories
sub-file-paths-append=ass:srt:sub:subs:subtitles
# make sure when seeking through mkv we try to display subs
demuxer-mkv-subtitle-preroll=yes
# use fonts embedded in the container if available
embeddedfonts=yes
# don't meddle with subtitle timing by default
sub-fix-timing=no
# do not blend subtitles, so that we can display them beyond the video borders
blend-subtitles=no

## look and style
sub-ass-override=scale
sub-scale=0.5
sub-font="Liberation Sans"
sub-font-size=70
# put them a bit above seekbar -- not necessary imo
# sub-margin-y=50
sub-color="#FFFFFFFF"
sub-border-color="#FF151515"
sub-border-size=6
sub-shadow-offset=1
sub-shadow-color="#33000000"
sub-spacing=0.5

### Audio

volume=80
volume-max=150
# find audio files even if slightly mismatched
audio-file-auto=fuzzy
# playing at different speed will pitch-correct
audio-pitch-correction=yes

### Video

hwdec=auto
profile=opengl-hq
opengl-early-flush=auto
opengl-pbo=no
# ever so slightly up saturation
saturation=12
# interpolation options, will take some more cpu
interpolation=yes
video-sync=display-resample

### Cache

# use cache if it seems like a networked connection
cache=auto

### youtube-dl setup

ytdl=yes
# never go beyond 1080p if it is avoidable
ytdl-format=(bestvideo[vcodec=vp9.2][height<=1080]/bestvideo[vcodec=vp9][fps>30][height<=1080]/bestvideo[vcodec=vp9][height<=1080]/bestvideo[fps>30][height<=1080]/bestvideo[height>720])+(bestaudio[acodec=opus]/bestaudio)/best

### Languages

# prefer english subtitles, use german if you have to
slang=en,eng,de,deu,ger
# prefer original language, use dub if necessary
alang=ja,jp,jpn,en,eng,de,deu,ger

### Protocol configuration

[network]
# don't wait for buffering to be complete to show the window
force-window=immediate
cache=yes
# create a huge cache to buffer most of videos
demuxer-max-bytes=3000MiB
demuxer-readahead-secs=500

[protocol.http]
profile=network

[protocol.https]
profile=network

[protocol.ytdl]
profile=network

[extension.gif]
interpolation=no
loop-file=yes

# for those yt playlists that are created in reverse order
[reverse]
ytdl-raw-options=playlist-reverse=

[lowquality]
scale=bilinear
cscale=ewa_lanczossharp
interpolation=no
video-sync=audio

[highquality]
scale=ewa_lanczossharp
cscale=ewa_lanczossharp
video-sync=display-resample
interpolation=yes
tscale=oversample

# default to hq
profile=highquality
