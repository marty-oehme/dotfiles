#!/usr/bin/env sh
# Ensure the directory for beets library exists or it errors out on startup

[ -e "${XDG_DATA_HOME:-$HOME/.local/share}/beets" ] || mkdir -p "${XDG_DATA_HOME:-$HOME/.local/share}/beets"
