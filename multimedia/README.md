# Multimedia module

[mpv](https://mpv.io) -- free, open-source, cross-platform media player
beets -- organize your music library
mopidy -- serve your music library
ncmpcpp -- actually play your music (and twist your tongue pronouncing it)

The largest modifications so far are definitely to mpv (described below),
with beets and mopidy being somewhat configured to my tastes as well.

mpv is set up to hopefully strike a balance between high quality playback, streaming with a reasonable speed and saving battery power.
It is set up to play both local files and streams from the web (especially youtube with playlisting and ad skipping), via a qutebrowser mapping if the corresponding module is installed.

* mpv by default does not come with a gui, this configuration uses [uosc](https://github.com/darsain/uosc) to enable a comfortable gui
* available subtitles are loaded and shown in a consistently high quality, as much as possible
* easy screenshot grabbing is possible, saving to a default folder (`~/pictures/screenshots`, can be changed at top of `mpv.conf`)
* audio is not meddled with too much, but should provide good default sound quality for pulseaudio
* playback position is *not* saved for every file by default, but can be used when quitting with shift-q (default mpv behavior)
* streaming video is optimized for a 1080p display, it will avoid qualities higher than that if possible
* default video is adjusted for playback during the day, in a normally lit room
* simple context menu (opened with `menu` key on keyboard) to load files, subtitles, chapters, and more
* newly defined keybindings, look in `input.conf` for their definitions
* when a battery is being discharged, mpv starts in a slightly lower quality but battery saving playback profile

## vim-like navigation

Scrolling through videos can be done with h and l (or H and L for larger steps),
scrolling through a playlist is accomplished with J and K,
and volume is controlled with j and k.

`a` controls playback speed, `s` cycles subtitles, `d` controls audio delay, `u` controls subtitle delay,
`f` remains fullscreen, `p` toggles pauses, `i` shows the playlist, `o` peeks at the timeline, `F5` takes a screenshot.

Additionally, pressing the `menu` button (between right `ctrl` and `alt`) will open a little context menu,
from which you can load other files, subtitles, chapters, switch audio tracks and take screenshots.

Most of this is (thanks to the hard work of the original script writers) easily customizable,
mostly from `input.conf` directly.

## playlist management

Uses the wonderful [playlistmanager](https://github.com/jonniek/mpv-playlistmanager) script to enable easy management of mpv playlists.
Enables manually or automatically sorting playlists, shuffling them, and saving them to files.

Additionally, it automatically populates streaming content with the video title instead of the url within the playlist.

## battery saving

When mpv ist started on a pc which it assumes to be running on battery power, it will automatically switch to a (slightly) lower quality mode,
foregoing some of the upscaling niceties integrated into the default high quality mode for (somewhat) better battery preservation.

This script uses the output of `/sys/class/power_supply/AC/online`, and thus depends on this file being readable to get its information.

It degrades gracefully, and simply keeps running in higher quality if the file is not readable.

## sponsorblock

The [minimal mpv-sponsorblock](https://codeberg.org/jouni/mpv_sponsorblock_minimal) script is included to enable automatically skipping many sponsorship segments integrated within youtube videos.

This works mostly fully automated, it checks the database and finds affected segments, which it then automatically skips over.

To toggle sponsorblock on or off just hit `b`, it will confirm the choice via osd.
