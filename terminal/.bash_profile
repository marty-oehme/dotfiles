#!/usr/bin/env bash
#
# ~/.bash_profile
#
# shellcheck disable=SC1090

# ensure bash is set up for its initial sourcing
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-"$HOME/.config"}

# load global sh env vars
[ -f "$XDG_CONFIG_HOME/sh/env" ] && source "$XDG_CONFIG_HOME/sh/env"
if [ -d "$XDG_CONFIG_HOME/sh/env.d" ]; then
    for _env in "$XDG_CONFIG_HOME/sh/env.d"/*.sh; do
        . "$_env"
    done
    unset _env
fi
[ -f "$XDG_CONFIG_HOME/bash/env" ] && source "$XDG_CONFIG_HOME/bash/env"
if [ -d "$XDG_CONFIG_HOME/bash/env.d" ]; then
    for _env in "$XDG_CONFIG_HOME/bash/env.d"/*.sh; do
        . "$_env"
    done
    unset _env
fi

# load profile files vars
[ -f "$XDG_CONFIG_HOME/sh/profile" ] && source "$XDG_CONFIG_HOME/sh/profile"
if [ -d "$XDG_CONFIG_HOME/sh/profile.d" ]; then
    for _profile in "$XDG_CONFIG_HOME/sh/profile.d"/*.sh; do
        . "$_profile"
    done
    unset _profile
fi
[ -f "$XDG_CONFIG_HOME/bash/profile" ] && source "$XDG_CONFIG_HOME/bash/profile"
if [ -d "$XDG_CONFIG_HOME/bash/profile.d" ]; then
    for _profile in "$XDG_CONFIG_HOME/bash/profile.d"/*.sh; do
        . "$_profile"
    done
    unset _profile
fi

# shellcheck disable=SC1090
[[ -f ~/.bashrc ]] && . ~/.bashrc
