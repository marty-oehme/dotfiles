setup() {
  fut="$BATS_TEST_DIRNAME/../tmux_attach_start"
}

teardown() {
  rm -rf $BATS_TMPDIR/xdg/*
}

@test "runs correctly if invoked without arguments" {
  run $fut
  echo "$BATS_TEST_DIRNAME/../tmux_attach_start"
  [ "$status" -eq 0 ]
}

@test "Errors out if passed a non-existent session file (and some session name)" {
  run $fut IDONT_EXIST_HERE.session sessionname
  echo "STATUS: $output"
  [ "$status" -eq 1 ]
  echo "OUTPUT: $output"
  [ "$output" = "can not source session file: 'IDONT_EXIST_HERE.session' does not exist." ]
}

@test "Runs without errors when session file exists in PWD" {
  touch $PWD/exists.session
  run $fut exists.session sessionname
  rm $PWD/exists.session

  [ "$status" -eq 0 ]
}

@test "Runs without errors when session file exists in XDG_CONFIG_HOME/tmux/sessions/" {
  export XDG_CONFIG_HOME=$BATS_TMPDIR
  mkdir -p $XDG_CONFIG_HOME/tmux/sessions
  sesdir=$XDG_CONFIG_HOME/tmux/sessions
  touch $sesdir/exists.session
  run $fut exists.session sessionname

  [ "$status" -eq 0 ]
}
