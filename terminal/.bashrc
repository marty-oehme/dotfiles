#!/usr/bin/env bash
#
# ~/.bashrc
#
# shellcheck disable=SC1090

CONFDIR="${XDG_CONFIG_HOME:-$HOME/.config}"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[ -f "$CONFDIR/sh/alias" ] && source "$CONFDIR/sh/alias"
# load additional aliases
if [ -d "$CONFDIR/sh/alias.d" ]; then
    for _alias in "$CONFDIR/sh/alias.d"/*.sh; do
        . "$_alias"
    done
    unset _alias
fi
if [ -d "$CONFDIR/bash/alias.d" ]; then
    for _alias in "$CONFDIR/bash/alias.d"/*.sh; do
        . "$_alias"
    done
    unset _alias
fi

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
