#!/usr/bin/env zsh
#

# make zsh source the correct directory
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-"$HOME/.config"}
ZDOTDIR="${XDG_CONFIG_HOME:-"$HOME/.config"}/zsh"
