#!/usr/bin/env sh

exist() { type "$1" >/dev/null 2>&1; }

# alias tmux to follow xdg-specification
# shellcheck disable=2139
alias tmux="tmux -f ${XDG_CONFIG_HOME:-$HOME/.config}/tmux/tmux.conf"

# fzf
if exist fzf; then
    # fzf select a tmux session to connect to, with pane preview
    alias tm='_fzf_tmux_attach_start_session'
    alias tl=tm
else
    alias tm='tmux_attach_start'
    # show a list of running tmux sessions
    alias tl='tmux list-sessions -F "#{session_name}" 2>/dev/null '
fi

_fzf_tmux_list_sessions() {
    tmux list-sessions -F "#{session_name}" 2>/dev/null | fzf \
        --layout=reverse \
        --height=50% \
        --border \
        --prompt="Session> " \
        --preview="tmux_pane_tree {}" \
        --preview-window=right:80% \
        --print-query
}

_fzf_tmux_attach_start_session() {
    if [ -z "$1" ]; then
        result=$(_fzf_tmux_list_sessions)
        case "$?" in
        0)
            # found a session, attaching
            tmux_attach_start "$(echo "$result" | tail --lines=1)"
            ;;
        1)
            # did not find a session, creating
            result=$(echo "$result" | head --lines=1)
            # if . was only thing entered, create one for current dir
            if [ "$result" = "." ]; then
                tmux_attach_start
                # create for query name
            else
                tmux_attach_start "$result"
            fi
            ;;
        esac
    else
        tmux_attach_start "$1"
    fi
}

unset choice
unset -f exist
