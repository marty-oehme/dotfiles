local wezterm = require("wezterm")
local io = require("io")
local os = require("os")
local act = wezterm.action

local function setup()
	local function isViProcess(pane)
		return pane:get_foreground_process_name():find("n?vim") ~= nil
	end

	local function conditionalActivatePane(window, pane, pane_direction, vim_direction)
		if isViProcess(pane) then
			window:perform_action(
				act.Multiple({
					act.SendKey({ key = "w", mods = "CTRL" }),
					act.SendKey({ key = vim_direction }),
				}),
				pane
			)
		else
			window:perform_action(act.ActivatePaneDirection(pane_direction), pane)
		end
	end

	wezterm.on("ActivatePaneDirection-right", function(window, pane)
		conditionalActivatePane(window, pane, "Right", "l")
	end)
	wezterm.on("ActivatePaneDirection-left", function(window, pane)
		conditionalActivatePane(window, pane, "Left", "h")
	end)
	wezterm.on("ActivatePaneDirection-up", function(window, pane)
		conditionalActivatePane(window, pane, "Up", "k")
	end)
	wezterm.on("ActivatePaneDirection-down", function(window, pane)
		conditionalActivatePane(window, pane, "Down", "j")
	end)

	-- Retrieve the current scrollback text and send to editor
	wezterm.on("edit-scrollback", function(window, pane)
		local viewport_text = pane:get_logical_lines_as_text(10000)

		-- Create a temporary file to pass to vim
		local name = os.tmpname()

		local f = io.open(name, "w+")
		if f == nil then
			return false
		end
		f:write(viewport_text)
		f:flush()
		f:close()

		-- Open a new window running vim and tell it to open the file
		window:perform_action(
			act.SpawnCommandInNewTab({
				args = { (os.getenv("EDITOR") or "vi"), name },
			}),
			pane
		)

		-- Wait time for vim to read the file before we remove it.
		wezterm.sleep_ms(1000)
		os.remove(name)
	end)

	wezterm.on("toggle-leader", function(window, pane)
		wezterm.log_info("toggling the leader")
		local overrides = window:get_config_overrides() or {}
		if not overrides.leader then
			wezterm.log_info("leader wasn't set")
			overrides.leader = { key = "s", mods = "SUPER" }
		else
			wezterm.log_info("leader was set")
			overrides.leader = nil
		end

		window:set_config_overrides(overrides)
	end)
end

return { setup = setup }
