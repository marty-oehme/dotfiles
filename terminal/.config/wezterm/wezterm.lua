local wezterm = require("wezterm")

local maps = require("maps")

require("statusbar").setup()
require("events").setup()

local function file_exists(name)
	local f = io.open(name, "r")
	if f ~= nil then
		io.close(f)
		return true
	else
		return false
	end
end

-- automatically reload colors file
local colorsfile = (os.getenv("XDG_STATE_HOME") or (os.getenv("HOME") .. "/.local/state")) .. "/wezterm/colors.toml"
local colors = {}
if file_exists(colorsfile) == true then
	wezterm.add_to_config_reload_watch_list(colorsfile)
	colors = wezterm.color.load_scheme(colorsfile)
end

local settings = {
	enable_wayland = true,
	hide_tab_bar_if_only_one_tab = true,
	use_fancy_tab_bar = false,
	tab_bar_at_bottom = true,
	window_padding = { left = 0, right = 0, top = 0, bottom = 0 },
	colors = colors,
	color_scheme = "Nord (base16)", -- will be overwritten by colors
	-- default_prog = {"nu"},
	scrollback_lines = 10000,
	font = wezterm.font("Iosevka Nerd Font"),
	-- add cursive italic font from Victor font for all weights
	font_rules = {
		{
			intensity = "Bold",
			italic = true,
			font = wezterm.font({
				family = "VictorMono Nerd Font",
				weight = "Bold",
				style = "Italic",
			}),
		},
		{
			italic = true,
			intensity = "Half",
			font = wezterm.font({
				family = "VictorMono Nerd Font",
				weight = "DemiBold",
				style = "Italic",
			}),
		},
		{
			italic = true,
			intensity = "Normal",
			font = wezterm.font({
				family = "VictorMono Nerd Font",
				style = "Italic",
			}),
		},
	},
	line_height = 1.0,
	leader = { key = "a", mods = "CTRL", timeout_milliseconds = 1500 },
	keys = maps.keys,
	key_tables = maps.key_tables,
	mouse_bindings = {
		{
			event = { Up = { streak = 1, button = "Left" } },
			mods = "NONE",
			action = wezterm.action.CompleteSelectionOrOpenLinkAtMouseCursor("ClipboardAndPrimarySelection"),
		},
	},
}
return settings
