local wezterm = require("wezterm")

local function basename(s)
	return string.gsub(s or "", "(.*[/\\])(.*)", "%2")
end

local SEPARATOR = " | "
local function setup()
	-- STATUSBAR
	-- show currently active key table in lower right status bar
	-- mimicing Vim modes
	wezterm.on("update-status", function(window, pane)
		local displayed = { left = {}, right = {} }
		local keytable = window:active_key_table()
		if keytable then
			displayed.left[#displayed.left + 1] = "MODE: " .. keytable
		end

		local workspace = window:active_workspace()
		if workspace and workspace ~= "default" then
			displayed.left[#displayed.left + 1] = "WORKSPACE: " .. workspace
		end

		local bat = ""
		for _, b in ipairs(wezterm.battery_info()) do
			bat = "🔋 " .. string.format("%.0f%%", b.state_of_charge * 100) .. " " .. b.state
		end
		displayed.right[#displayed.right + 1] = bat

		local currentprogram = pane:get_foreground_process_name()
		displayed.right[#displayed.right + 1] = basename(currentprogram)

		local statusleft = ""
		for _, v in ipairs(displayed.left) do
			statusleft = statusleft .. v .. SEPARATOR
		end
		local statusright = ""
		for _, v in ipairs(displayed.right) do
			statusright = statusright .. v .. SEPARATOR
		end

		window:set_left_status(statusleft or "")
		window:set_right_status(statusright or "")
	end)
end

return { setup = setup }
