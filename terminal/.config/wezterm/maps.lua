local wezterm = require("wezterm")
local act = wezterm.action

local keys = {
	{ key = "O", mods = "CTRL", action = act.ShowDebugOverlay },

	{ key = "[", mods = "CTRL", action = act.ScrollToPrompt(-1) },
	{ key = "]", mods = "CTRL", action = act.ScrollToPrompt(1) },
	{ -- vertical pane
		key = "\\",
		mods = "LEADER",
		action = act.SplitHorizontal({ domain = "CurrentPaneDomain" }),
	},
	{ -- horizontal pane
		key = "-",
		mods = "LEADER",
		action = act.SplitVertical({ domain = "CurrentPaneDomain" }),
	}, -- pane movement keys
	{
		key = "h",
		mods = "CTRL",
		action = act.EmitEvent("ActivatePaneDirection-left"),
	},
	{
		key = "j",
		mods = "CTRL",
		action = act.EmitEvent("ActivatePaneDirection-down"),
	},
	{
		key = "k",
		mods = "CTRL",
		action = act.EmitEvent("ActivatePaneDirection-up"),
	},
	{
		key = "l",
		mods = "CTRL",
		action = act.EmitEvent("ActivatePaneDirection-right"),
	},
	{ key = "z", mods = "LEADER", action = act.TogglePaneZoomState },
	{ key = " ", mods = "LEADER", action = act.RotatePanes("Clockwise") },
	{ key = "q", mods = "LEADER", action = act.PaneSelect({ mode = "Activate" }) },
	{
		key = "Q",
		mods = "LEADER",
		action = act.PaneSelect({ mode = "SwapWithActive" }),
	},
	{ key = "c", mods = "LEADER", action = act.SpawnTab("CurrentPaneDomain") },
	{ key = ",", mods = "LEADER", action = act.MoveTabRelative(-1) },
	{ key = ".", mods = "LEADER", action = act.MoveTabRelative(1) }, -- workspace selection
	{
		key = "s",
		mods = "LEADER",
		action = act.ShowLauncherArgs({ flags = "FUZZY|WORKSPACES" }),
	},
	{ key = "t", mods = "LEADER", action = act.ShowTabNavigator },
	{ key = "[", mods = "LEADER", action = act.ActivateCopyMode },
	{
		key = "r",
		mods = "LEADER",
		action = act.ActivateKeyTable({
			name = "resize_pane",
			one_shot = false,
			timeout_milliseconds = 2000,
			replace_current = true,
		}),
	},
	{ key = "f", mods = "LEADER", action = act.QuickSelect },
	{
		key = "F",
		mods = "LEADER",
		action = wezterm.action.QuickSelectArgs({
			patterns = { "https?://\\S+" },
			action = wezterm.action_callback(function(window, pane)
				local url = window:get_selection_text_for_pane(pane)
				wezterm.log_info("opening: " .. url)
				wezterm.open_with(url)
			end),
		}),
	},
	{
		key = "/",
		mods = "LEADER",
		action = act.Search("CurrentSelectionOrEmptyString"),
	},
	{
		key = "b",
		mods = "LEADER",
		action = act.ActivateKeyTable({
			name = "scroll_mode",
			one_shot = false,
			replace_current = true,
			timeout_milliseconds = 15000,
		}),
	},
	{ key = "e", mods = "LEADER", action = act.EmitEvent("edit-scrollback") },
	{
		key = "l",
		mods = "LEADER",
		action = act.EmitEvent("ActivatePaneDirection-Right"),
	},
	{ key = "a", mods = "CTRL|ALT", action = act.EmitEvent("toggle-leader") },
}
-- Leader + number to activate that tab
for i = 1, 8 do
	table.insert(keys, {
		key = tostring(i),
		mods = "LEADER",
		action = act.ActivateTab(i - 1),
	})
end
-- key table sub modes
local key_tables = {
	-- mode to change size of any panes
	resize_pane = {
		{ key = "h", action = act.AdjustPaneSize({ "Left", 1 }) },
		{ key = "l", action = act.AdjustPaneSize({ "Right", 1 }) },
		{ key = "k", action = act.AdjustPaneSize({ "Up", 1 }) },
		{ key = "j", action = act.AdjustPaneSize({ "Down", 1 }) },
		{ key = "H", action = act.AdjustPaneSize({ "Left", 10 }) },
		{ key = "L", action = act.AdjustPaneSize({ "Right", 10 }) },
		{ key = "K", action = act.AdjustPaneSize({ "Up", 10 }) },
		{ key = "J", action = act.AdjustPaneSize({ "Down", 10 }) },
		{ key = "Escape", action = "PopKeyTable" },
	},
	scroll_mode = {
		{ key = "y", mods = "CTRL", action = act.ScrollByLine(-1) },
		{ key = "e", mods = "CTRL", action = act.ScrollByLine(1) },
		{ key = "f", mods = "CTRL", action = act.ScrollByPage(1) },
		{ key = "b", mods = "CTRL", action = act.ScrollByPage(-1) },
		{ key = "d", mods = "CTRL", action = act.ScrollByPage(0.5) },
		{ key = "u", mods = "CTRL", action = act.ScrollByPage(-0.5) },
		{ key = "g", mods = "CTRL", action = act.ScrollToTop },
		{ key = "G", mods = "CTRL", action = act.ScrollToBottom },
		{ key = "Escape", action = "PopKeyTable" },
	},
}
return { keys = keys, key_tables = key_tables }
