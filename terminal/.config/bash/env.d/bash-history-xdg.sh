#!/usr/bin/env bash

export HISTFILE="${XDG_STATE_HOME}/bash/history"

# bashrc, bash_profile, bash_logout currently unsupported
# see  https://savannah.gnu.org/support/?108134
