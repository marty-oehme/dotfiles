#!/usr/bin/env zsh
#

CONFDIR="${XDG_CONFIG_HOME:-$HOME/.config}"
ZSHCONFDIR="$CONFDIR/zsh"

# load completion, extended zsh moving syntax, zle edit in vim (or other $EDITOR) possibility
autoload -Uz compinit zmv edit-command-line promptinit

# Set completion style
# The following lines were added by compinstall
zstyle ':completion:*' completer _complete _ignored _approximate
zstyle ':completion:*' matcher-list '' '+m:{[:lower:]}={[:upper:]} r:|[._-]=** r:|=**'
zstyle ':completion:*' menu select=1
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle :compinstall filename "$ZSHCONFDIR/.zshrc"
compinit
# End of lines added by compinstall

# load plugins
[ -e /usr/share/oh-my-zsh/plugins/colored-man-pages/colored-man-pages.plugin.zsh ] && source /usr/share/oh-my-zsh/plugins/colored-man-pages/colored-man-pages.plugin.zsh
[ -e /usr/share/oh-my-zsh/plugins/command-not-found/command-not-found.plugin.zsh ] && source /usr/share/oh-my-zsh/plugins/command-not-found/command-not-found.plugin.zsh
[ -e /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
#source /usr/share/nvm/init-nvm.sh
## find the correct installed tab-completion version
PLUG_FOLDER="/usr/share/zsh/plugins"
[ -e $PLUG_FOLDER/fzf-tab/fzf-tab.plugin.zsh ] && source $PLUG_FOLDER/fzf-tab/fzf-tab.plugin.zsh
[ -e $PLUG_FOLDER/fzf-tab-bin-git/fzf-tab.plugin.zsh ] && source $PLUG_FOLDER/fzf-tab-bin-git/fzf-tab.plugin.zsh
# these need to be sourced after fzf-tab
[ -e $PLUG_FOLDER/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh ] && source $PLUG_FOLDER/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
[ -e $PLUG_FOLDER/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && source $PLUG_FOLDER/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[ -e $PLUG_FOLDER/alias-tips/alias-tips.plugin.zsh ] && source $PLUG_FOLDER/alias-tips/alias-tips.plugin.zsh
[ -e $PLUG_FOLDER/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh ] && source $PLUG_FOLDER/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
unset PLUG_FOLDER

# simple fzf-tab settings
zstyle ":fzf-tab:*" fzf-flags "--ansi" "--expect='$continuous_trigger,$print_query'" "--color=hl:$(($#headers == 0 ? 108 : 255))" "--nth=2,3" "--layout=reverse" "--height=${FZF_TMUX_HEIGHT:-75%}" "--tiebreak=begin" "-m" "--bind=tab:down,btab:up,change:top,ctrl-space:toggle" "--cycle" "--query=$query" "--header-lines=$#headers" "--print-query"

zstyle ':fzf-tab:*' fzf-command fzf
# format colorful groups for different completion actions
zstyle ':completion:*:descriptions' format '[%d]'
zstyle ':fzf-tab:*' show-group brief
# use input as query string when completing zlua
zstyle ':fzf-tab:complete:_zlua:*' query-string input
# (experimental, may change in the future)
# some boilerplate code to define the variable `extract` which will be used later
# please remember to copy them
local extract="
# trim input(what you select)
local in=\${\${\"\$(<{f})\"%\$'\0'*}#*\$'\0'}
# get ctxt for current completion(some thing before or after the current word)
local -A ctxt=(\"\${(@ps:\2:)CTXT}\")
# real path
local realpath=\${ctxt[IPREFIX]}\${ctxt[hpre]}\$in
realpath=\${(Qe)~realpath}
"
# give a preview of commandline arguments when completing `kill`
zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,user,comm,cmd -w -w"
zstyle ':fzf-tab:complete:kill:argument-rest' extra-opts --preview=$extract'ps --pid=$in[(w)1] -o cmd --no-headers -w -w' --preview-window=down:3:wrap
# give a preview of directory by exa when completing cd
zstyle ':fzf-tab:complete:cd:*' extra-opts --preview=$extract'exa -1 --color=always $realpath'

# pure prompt init
promptinit
# make it single line
prompt_newline='%666v'
# show git stash status as a ≡
zstyle :prompt:pure:git:stash show yes
# show hostname if we are in a distrobox environment
if [ -n "$DISTROBOX_ENTER_PATH" ] && [ -f /run/.containerenv ]; then
    local container=lxc
fi
prompt pure

# shellcheck source=alias
[ -f "$CONFDIR/sh/alias" ] && source "$CONFDIR/sh/alias"
# load additional aliases
if [ -d "$CONFDIR/sh/alias.d" ]; then
    for _alias in "$CONFDIR/sh/alias.d"/*.sh; do
        source "$_alias"
    done
    unset _alias
fi
# shellcheck source=alias
[ -f "$ZSHCONFDIR/alias" ] && source "$ZSHCONFDIR/alias"
if [ -d "$ZSHCONFDIR/alias.d" ]; then
    for _alias in "$ZSHCONFDIR/alias.d"/*.sh; do
        source "$_alias"
    done
    unset _alias
fi

# Correct spelling for commands
setopt correct
# turn off the infernal correctall for filenames
unsetopt correctall
# Enable command auto-correction.
ENABLE_CORRECTION="true"
# allow moving through directories without prepending cd
setopt autocd

# Speed up autocomplete, force prefix mapping
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "${XDG_CACHE_HOME:-$HOME/.cache}/zshcompcache"

### history
## Set ZSH History defaults
setopt append_history
setopt extended_history # record command start time
setopt hist_ignore_dups
setopt hist_ignore_all_dups # do not put duplicated command into history list
setopt hist_save_no_dups    # do not save duplicated command
setopt hist_expire_dups_first
setopt hist_ignore_space
setopt hist_reduce_blanks # remove unnecessary blanks
setopt hist_verify
setopt inc_append_history_time # append command to history file immediately after execution
setopt share_history           # Share your history across all your terminal windows
setopt pushd_ignore_dups
#setopt pushd_silent
export HISTSIZE=1000000
export SAVEHIST=1000000
export HISTFILE="$XDG_DATA_HOME/zsh_history"
export HISTORY_IGNORE="(jrnl *|l *|ls *|z *|cd *|cd -|pwd|pwd *|exit|date)"

## Set ZSH History aliases
# Show the top 5 commands used in recent history
history_top() {
    history | awk "{a[\$2]++} END{for(i in a){printf \"%5d\t%s\n\",a[i],i}}" | sort -rn | head
}
# Display timestamped recent command history
alias history="fc -l -d -D"

### Glob Alias expansion
# Expand aliases inline - see http://blog.patshead.com/2012/11/automatically-expaning-zsh-global-aliases---simplified.html
globalias() {
    if [[ $LBUFFER =~ [A-Z0-9]+$ ]]; then
        zle _expand_alias
        zle expand-word
    fi
    zle self-insert
}
zle -N globalias
bindkey " " globalias
bindkey "^ " magic-space           # control-space to bypass completion
bindkey -M isearch " " magic-space # normal space during searches

# allow inserting previous arguments in the current zle input
# see https://stackoverflow.com/a/34861762
bindkey "^[." insert-last-word

# VIM MODE for the shell
# enable vim mode on pressing escape
bindkey -v
# remove the delay for switching modes
export KEYTIMEOUT=1
# space puts a space, even in cmd mode
bindkey -a ' ' magic-space
# always allow backspace/delete to remove letters
bindkey '^?' backward-delete-char
bindkey -a '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey -a '^h' backward-delete-char
# and forward with delete
bindkey '^[[3~' delete-char
bindkey -a '^[[3~' delete-char
# always allow removing words with <c-w>
bindkey -a '^w' backward-kill-word
# enable cycling through previous commands with <c-p/n>
bindkey '^P' history-beginning-search-backward
bindkey '^N' history-beginning-search-forward

# search history backwards <c-r>
bindkey '^o' history-incremental-search-backward
# cycle through history results
bindkey -M isearch '^P' history-incremental-search-backward
bindkey -M isearch '^N' history-incremental-search-forward

# Send command to editor and back for execution
zle -N edit-command-line
bindkey '^e' edit-command-line
bindkey -M vicmd '^e' edit-command-line

# Deduplicate PATH - remove any duplicate entries from PATH
# from: https://unix.stackexchange.com/questions/40749/remove-duplicate-path-entries-with-awk-command
get_var() {
    eval 'printf "%s\n" "${'"$1"'}"'
}
set_var() {
    eval "$1=\"\$2\""
}
dedup_pathvar() {
    pathvar_name="$1"
    pathvar_value="$(get_var "$pathvar_name")"
    deduped_path="$(perl -e 'print join(":",grep { not $seen{$_}++ } split(/:/, $ARGV[0]))' "$pathvar_value")"
    set_var "$pathvar_name" "$deduped_path"
}
dedup_pathvar PATH
dedup_pathvar MANPATH

unset CONFDIR
unset ZSHCONFDIR
