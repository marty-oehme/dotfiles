#!/usr/bin/env zsh

[ -f "$XDG_CONFIG_HOME/sh/profile" ] && . "$XDG_CONFIG_HOME/sh/profile"
# .zlogin
#
if [ -d "$XDG_CONFIG_HOME/sh/profile.d" ]; then
  for file in "$XDG_CONFIG_HOME/sh/profile.d"/*.sh; do
    # shellcheck disable=1090
    source "$file"
  done
  unset file
fi
if [ -d "$XDG_CONFIG_HOME/zsh/profile.d" ]; then
  for file in "$XDG_CONFIG_HOME/zsh/profile.d"/*.sh; do
    # shellcheck disable=1090
    source "$file"
  done
  unset file
fi
