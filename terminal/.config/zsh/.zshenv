#!/usr/bin/env zsh
#

# load zsh specific env vars
if [ -d "$XDG_CONFIG_HOME/zsh/env.d" ]; then
  for _env in "$XDG_CONFIG_HOME/zsh/env.d"/*.zsh; do
    . "$_env"
  done
  unset _env
fi

# add XDG_CONFIG_HOME/zsh/completions to fpath
# zsh uses it to look for completions; throw all
# completions in these dotfiles in there
fpath=("$XDG_CONFIG_HOME/zsh/completions" $fpath)
