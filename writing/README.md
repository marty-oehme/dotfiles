# Writing module

[bibtex](https://en.wikipedia.org/wiki/BibTeX) - plain-text reference management
[zathura](git.pwmt.org) - keyboard-driven PDF reading

This readme is a little out of date, as are the scripts below.
They are old, I used them during my time as a student and they are probably written terribly.
But I still think there is value in them, hence not removing them from the repo just yet.

## bib-due

The `bib-due` script depends on (gnu) grep, awk, and sed, date if using date filtering capabilities. It is currently written in a rather haphazard way, and prone to breakage.
On the other hand, it does what it's supposed to do: list bibtex entries which have their due-date coming up.

The script needs bibtex entries to be marked with two fields: `due`, containing a due date (ideally in YYYY-MM-DD format, for easy sorting), and `priority` containing a read priority. It will also, by default attempt to grab the values of the fields `author` and `title`, as well as the name of the bibtex key of the entry.

It can be invoked with the path to a bibtex file `bib-due path/to/library.bib`, and will gather the entries from the respective file. It can be invoked without an argument if the environment variable `$BIBFILE` is declared (pointing to a bibtex file).

Example output looks as follows:

![bib-due example output](.assets/bibtex/list.png)

The output can then be filtered further through other programs.

Bib-due itself allows 2 filtering options: *until* a certain date (`-u`), and *at least* a certain priority (`-p`).

Using priority is relatively self-explanatory: 1 is the highest priority, 3 the lowest (technically, no priority is the lowest). Choosing `-p3` means priority 1, 2, and 3 will be displayed. Choosing `-p1` will only display the highest priority items.

Using the date works as a cut-off for the future, and it uses gnu `date` to calculate the correct date. That makes things like `-u 'fri this week'` possible, showing only upcoming items until the end of the week. Read the `date` manual for more information.
There will likely not be a new option for filtering dates *from* a certain point forward since I don't need it and before implementing more stuff what's there should be more solid. (and read your damn overdue texts!)

Again, this script will (for now[^time]) break when bibtex files are formatted in any way other than what it expects.
An example of a working entry:

[^time]: And probably for some time since I don't see myself sinking too much more time into this in the near future. I actually need to get some of the upcoming readings done that I can now list! 🙂

```
@InBook{Borhi2016,
  author    = {László Borhi},
  chapter   = {1956: Self-Liberation},
  pages     = {117--137},
  publisher = {Indiana University Press},
  title     = {Dealing with dictators: the {United States}, {Hungary}, and {East Central Europe}, 1942-1989},
  year      = {2016},
  due       = {2020-05-07},
  file      = {:Borhi2016 - Dealing with Dictators_ the United States, Hungary, and East Central Europe, 1942 1989.pdf:PDF},
  pagetotal = {564},
  priority  = {prio1},
  timestamp = {2020-05-08},
}
```

Important fields are author, title, due, priority. These need to exist, and need to be ordered alphabetically. Otherwise there will probably be some breakage.

## rofi-bib-due

The `rofi-bib-due` script utilizes the `bib-due` script and depends on an existing installed `rofi` module (see [here](rofi/)).
On invocation, it creates a list of upcoming readings, and allows selecting one of the readings. The selected reading will be passed along to `$FILEREADER` if it is declared, falling back to `xdg-open` if not.

Currently, the path to the reading pdf is hard-coded to be `path/to/bibtex.bib/pdf`, and the name has to begin with the exact bibtex key; otherwise the script will not be able to find the pdf.

An example of the script in action: (window size has been reduced for the recording, cutting off most entry names)

![rofi-bib-due demonstration](.assets/bibtex/rofi.gif)
