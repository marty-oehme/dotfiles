#!/usr/bin/env sh
#

n() {
    if [ $# -eq 0 ]; then
        zk edit -i
    else
        zk "${@}"
    fi
}
