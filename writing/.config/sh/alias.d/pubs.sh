#!/usr/bin/env bash

pubs() {
    command pubs --config "$XDG_CONFIG_HOME/pubs/pubsrc" "${@}"
    if [ -f "$BIBFILE" ]; then
        command pubs export > "$BIBFILE"
    fi
}
