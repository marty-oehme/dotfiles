#!/usr/bin/env sh
# Settings for pass-ssh extension

alias pass-ssh='pass ssh --fzf -d ~/.ssh/keys --ssh-t ed25519'
