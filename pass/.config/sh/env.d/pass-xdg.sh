#!/usr/bin/env sh
# Make pass comply with xdg base dir specification,
# usually password store will be found in ~/.local/share/pass

export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
export PASSWORD_STORE_ENABLE_EXTENSIONS=true
