# pass

[pass](https://www.passwordstore.org/) - command-line password manager

The pass module tries to make interacting with the basic necessities of pass painless and quick -- it is focused on accessing secrets, and quickly auto-filling forms or copying secrets to the clipboard.

The configuration of pass is relatively standard, it tries to follow XDG_specifications, putting the default pass store into `~/.local/share/pass`.

The module is based on the original implementation for [gopass](https://github.com/gopasspw/gopass) which I decided to drop after repeated [minor version bumps](https://github.com/gopasspw/gopass/issues/1566) with breaking api changes.
With the additional new direction away from pass compatibility by gopass I ultimately decided to fall back to the more stable api of pass,
foregoing some of its more advanced quality of life features.

The general functionality of password management should not be affected.
The last working `rofi-gopass` implementation before the switch can be found [here](https://gitlab.com/marty-oehme/dotfiles/-/tree/7456bb14ab18ee09bd8b9332faa43d35b22e9e55/gopass).

## rofi-pass

To make accessing secrets easy, it uses the `rofi-pass` script, which creates a small rofi menu displaying all your secrets (names only), and from which you have quick access to copy, fill, or open the individual entries. An example of the menu in action:

![rofi-gopass demonstration](.assets/gopass/rofi-menu.gif)

There are several keybindings available, to either fill or copy to clipboard the username, password, or open the full view to an individual entry. For every send to clipboard action, the clipboard will be automatically cleared after the time specified in your pass settings. For auto-filling to work correctly, the username field will (by default) have to be highlighted on the entry webpage or form.

| keybinding         | function                                   | setting name            |
| --------           | --------                                   | ----------              |
| Return             | Auto-fill username & password              | KEY_AUTOFILL            |
| Alt+Return         | Open individual entry                      | KEY_OPEN_ENTRY          |
| Alt+u              | Auto-fill username                         | KEY_FILL_USER           |
| Alt+p              | Auto-fill password                         | KEY_FILL_PASS           |
| Ctrl+Alt+u         | Send username to clipboard                 | KEY_CLIP_USER           |
| Ctrl+Alt+p         | Send password to clipboard                 | KEY_CLIP_PASS           |
| From opened entry: |                                            |                         |
| Return             | Auto-fill selected field                   | KEY_ENTRYMENU_FILL      |
| Alt+Return         | Send selected field to clipboard           | KEY_ENTRYMENU_CLIP      |
| Alt+s              | Reveal contents of the password field      | KEY_ENTRYMENU_SHOWFIELD |
| Alt+BackSpace      | Close individual entry, return to overview | KEY_ENTRYMENU_QUIT      |

These keys, as well as the additional configuration can be changed by setting the corresponding environment variable, through a configuration file, or at the top of the script file itself. The script tries to follow xdg-specification, meaning it looks for a configuration file in the following directories (in descending order):

* `XDG_CONFIG_HOME/rofi-pass/rofi-pass.conf`
* `~/.config/rofi-pass/rofi-pass.conf`
* `~/.rofi-pass.conf`
* `/etc/rofi-pass.conf`

or, alternatively, a custom directory if the `RP_CONFIGURATION_FILE` variable points to a configuration file.

To use environment variables to configure any of these options or keys, prefix them with `RP_`, so that e.g. `KEY_AUTOFILL` becomes `RP_KEY_AUTOFILL`. Environment variables take precedence over configuration file settings.

Additional configuration options:

* `AUTOFILL_BACKEND`
  :sets the auto-filling tool used, only tested with `xdotool` currently.

* `AUTOFILL_CHAIN`
  :sets the chain of keys that should be sent to auto-fill an entry. Can use the following special fields: `:tab`, `:space`, `:return`, `username`, `password`.

  The default chain is `username :tab password`, which will enter the username, simulate the tab-key to switch from the username to the password field, and enter the password. This can be changed to suit your needs. To, for example, log in fully automatically at the end of the sequence, change it to `username :tab password :return`, and there will be no further user input for the login required.

* `AUTOFILL_DELAY`
  :sets the time for xdotool to wait in-between simulated actions, if some letters appear missing or the fields are not switched between quickly enough, it can usually be fixed by increasing this delay (though typing will also take longer)

* `PASS_USERNAME_FIELD`
  :sets the name of the field in pass secrets which contain the username. Usually, the default setting should be fine (it will look for `user`, then `username`, then `login`) but custom field names can be supplied. If multiple field names are given, it will use the first supplied field name a secret contains.
