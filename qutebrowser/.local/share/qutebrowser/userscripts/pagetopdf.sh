#! /usr/bin/bash
# Download current page as pdf file

# use title of current page / selected text as filename
filename=${QUTE_SELECTED_TEXT:-$QUTE_TITLE}
# revert to default name if nothing else is set; remove special chars
filename=$(echo "${filename:-downloaded.pdf}" | sed 's/[<>,/]/_/g')

# print to pdf
echo "print --pdf '$QUTE_DOWNLOAD_DIR/${1:-$filename}.pdf'" >>"$QUTE_FIFO"
