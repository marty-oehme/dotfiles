#! /usr/bin/bash
#
# Send current page/link to a wallabag instance.
#
# Can be used for sending the current page via:
# :spawn --userscript wallabag_add.sh
# for sending an arbitrary page passed as argument:
# :spawn --userscript wallabag_add.sh https://myinterestingtext.com
# or for sending a hinted link:
# :hint links userscript wallabag_add.sh
#
# Configure your wallabag instance with this:
WALLABAG_INSTANCE="https://read.martyoeh.me"

# only works for wallabag v2.*
if [ "$#" -gt 0 ]; then
    echo "open -b -r $WALLABAG_INSTANCE/bookmarklet?url=$*" >>"$QUTE_FIFO"
else
    echo "open -b -r $WALLABAG_INSTANCE/bookmarklet?url=$QUTE_URL" >>"$QUTE_FIFO"
fi
