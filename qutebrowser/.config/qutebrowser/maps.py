# Key mappings
# 'Leader' key binding -- for general personal maps
leader = "<Space>"
# 'LocalLeader' key binding -- intended for 'in-page' personal maps
lleader = ","

## CHANGED DEFAULTS

# rebind moving tabs to free for download
config.bind("gG", "tab-give")
# switch binds for scroll-marks and quick-/book-marks
config.bind("m", "mode-enter set_mark")
config.bind("`", "quickmark-save")
config.bind("~", "bookmark-add")

config.bind("<Ctrl-J>", "command-history-next", mode="command")
config.bind("<Ctrl-K>", "command-history-prev", mode="command")
config.bind("<Ctrl-P>", "completion-item-focus prev", mode="command")
config.bind("<Ctrl-N>", "completion-item-focus next", mode="command")
config.bind("<Alt-Backspace>", "rl-backward-kill-word", mode="command")
config.bind("<Alt-D>", "rl-kill-word", mode="command")
config.bind("<Alt-F>", "rl-forward-word", mode="command")
config.bind("<Ctrl-?>", "rl-delete-char", mode="command")
config.bind("<Ctrl-A>", "rl-beginning-of-line", mode="command")
config.bind("<Ctrl-B>", "rl-backward-char", mode="command")
config.bind("<Ctrl-C>", "completion-item-yank", mode="command")
config.bind("<Ctrl-D>", "completion-item-del", mode="command")
config.bind("<Ctrl-E>", "rl-end-of-line", mode="command")
config.bind("<Ctrl-F>", "rl-forward-char", mode="command")
config.bind("<Ctrl-H>", "rl-backward-delete-char", mode="command")
config.bind("<Ctrl-Shift-Tab>", "completion-item-focus prev-category", mode="command")
config.bind("<Ctrl-Tab>", "completion-item-focus next-category", mode="command")
config.bind("<Ctrl-U>", "rl-unix-line-discard", mode="command")
config.bind("<Ctrl-W>", "rl-unix-word-rubout", mode="command")
config.bind("<Ctrl-Y>", "rl-yank", mode="command")

## ADDED
# toggles ('cycles') between tabs always showing, or only when switching between them
config.bind(
    leader + "tt",
    "config-cycle -t tabs.show multiple switching ;; config-cycle -t statusbar.show always in-mode",
)
config.bind(leader + "th", "set tabs.position bottom")
config.bind(leader + "tH", "set tabs.position top")
config.bind(leader + "tv", "set tabs.position right")
config.bind(leader + "tV", "set tabs.position left")

# [M]edia shortcuts - watch, queue, download media
# bind mpv to play the current page/links, using a single instance which queues the next link passed
config.bind(lleader + "M", "spawn umpv {url}")
config.bind(lleader + "m", "hint links spawn umpv {hint-url}")
config.bind(lleader + "I", "spawn uoeia -v imv {url}")
config.bind(lleader + "i", "hint links spawn uoeia -v imv {hint-url}")
config.bind(lleader + "<Ctrl-i>", "hint images spawn uoeia -v imv {hint-url}")

# bind youtube-dl to download the current page/links
config.bind(lleader + "dM", "spawn vidl {url}")
config.bind(
    lleader + "dm",
    "hint --rapid links spawn vidl {hint-url}",
)
# Download shortcuts
config.bind(lleader + "dd", "download", mode="normal")
config.bind(lleader + "dp", "save-to-pdf", mode="normal")

# open last download
config.bind("gD", "recent-downloads", mode="normal")

config.bind('"w', "add-wallabag", mode="normal")  # add current page to wallabag
config.bind(";w", "hint links run add-wallabag")  # add link to wallabag
config.bind('"s', "add-shaarli", mode="normal")

config.bind('"a', "send-to-archive", mode="normal")

config.bind('"t', "translate-page-google", mode="normal")
config.bind('"T', "translate-selection-google", mode="normal")

config.bind('"q', "show-qr")

config.bind('"p', "send-to-scihub", mode="normal")  # view current page doi on scihub
config.bind(";p", "hint links run send-to-scihub")  # view linked doi on scihub

config.bind(lleader + "r", "readable", mode="normal")

# set stylesheets for the browser to use
config.bind(
    leader + "s",
    "config-cycle content.user_stylesheets " + 'stylesheets/stylesheet.css ""',
)

config.bind(leader + "a", "set-cmd-text -s :taskadd")

# Enable and disable javascript
config.bind(leader + "js", "config-cycle content.javascript.enabled true false")

# Reload configuration
config.bind(leader + "VV", "config-source")

# Enable code snippet hinting mode
config.bind(";c", "hint code userscript code_select.py")
# first looks if it's a gemini link and then fall back to http
config.bind(";g", "hint links userscript qute-gemini")
config.bind(";G", "hint links userscript qute-gemini-tab")
