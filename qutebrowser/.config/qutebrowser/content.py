c.content.blocking.enabled = True
c.content.blocking.method = "both"
c.content.blocking.adblock.lists = [
    "https://www.malwaredomainlist.com/hostslist/hosts.txt",
    "https://someonewhocares.org/hosts/hosts",
    "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext",
    "https://secure.fanboy.co.nz/fanboy-cookiemonster.txt",
]
c.content.blocking.hosts.lists = [
    "https://winhelp2002.mvps.org/hosts.zip",
    "https://malwaredomains.lehigh.edu/files/justdomains.zip",
]
c.content.blocking.whitelist = ["piwik.org"]
c.content.autoplay = False
c.content.prefers_reduced_motion = True
c.content.pdfjs = False
c.content.javascript.enabled = False
js_whitelist = [
    "*://*.youtube.com/*",
    "*://127.0.0.1/*",
    "*://asciinema.org/*",
    "*://calendar.google.com/*",
    "*://darksky.net/*",
    "*://deepl.com/*",
    "*://duckduckgo.com/*",
    "*://fosstodon.org/*",
    "*://github.com/*",
    "*://gitlab.com/*",
    "*://localhost/*",
    "*://mail.google.com/*",
    "*://maps.google.com/*",
    "*://*.martyoeh.me/*",
    "*://news.ycombinator.com/*",
    "*://old.reddit.com/*",
    "*://translate.google.com/*",
    "chrome://*/*",
    "file://*",
    "qute://*/*",
]
for page in js_whitelist:
    with config.pattern(page) as p:
        p.content.javascript.enabled = True
