import random
import re
from qutebrowser.api import interceptor
from qutebrowser.extensions.interceptors import RedirectException
from qutebrowser.utils import message

def fixScribePath(url):
    """ Fix external medium blog to scribe translation.
    Some paths from medium will go through a 'global identity'
    path which messes up the actual url path we want to go
    to and puts it in queries. This puts it back on the path.
    """
    new_path = f"{url.path()}{url.query()}"
    url.setQuery("")
    url.setPath(re.sub(r"m/global-identity-2redirectUrl=", "", new_path))

redirects = {
    "youtube": {
        "source": ["youtube.com"],
        "target": [
            "yt.oelrichsgarcia.de",
            "invidious.weblibre.org",
            "invidious.dhusch.de",
            "iv.ggtyler.dev",
            "invidious.baczek.me",
            "yt.funami.tech",
            "iv.melmac.space",
            "invidious.silur.me",
            "inv.riverside.rocks",
            "invidious.lidarshield.cloud",
            "invidious.flokinet.to",
            "invidious.snopyta.org",
            "invidious.kavin.rocks",
            "vid.puffyan.us",
            "yt.artemislena.eu",
            "invidious.nerdvpn.de",
            "invidious.tiekoetter.com",
            "invidious.namazso.eu",
            "inv.vern.cc",
            "yewtu.be",
            "inv.bp.projectsegfau.lt",
            "invidious.epicsite.xyz",
            "y.com.sb",
            "invidious.sethforprivacy.com",
        ],
    },
    "lbry": {
        "source": ["odysee.com"],
        "target": [
            "lbry.bcow.xyz",
            "odysee.076.ne.jp",
            "librarian.pussthecat.org",
            "lbry.mutahar.rocks",
            "lbry.vern.cc",
        ],
    },
    "reddit": {
        "source": ["reddit.com"],
        "target": [
            "td.vern.cc",
            "teddit.adminforge.de",
            "teddit.artemislena.eu",
            "teddit.bus-hit.me",
            "teddit.hostux.net",
            "teddit.namazso.eu",
            "teddit.net",
            "teddit.pussthecat.org",
            "teddit.sethforprivacy.com",
            "teddit.totaldarkness.net",
            "teddit.zaggy.nl",
        ],
    },
    "twitter": {
        "source": ["twitter.com"],
        "target": [
            "nitter.net",
            "nitter.42l.fr",
            "nitter.fdn.fr",
            "nitter.1d4.us",
            "nitter.kavin.rocks",
            "nitter.unixfox.eu",
            "nitter.namazso.eu",
            "nitter.moomoo.me",
            "bird.trom.tf",
            "nitter.it",
            "twitter.censors.us",
            "nitter.grimneko.de",
            "twitter.076.ne.jp",
            "n.l5.ca",
            "unofficialbird.com",
            "nitter.ungovernable.men",
        ],
    },
    "imdb": {
        "source": ["imdb.com"],
        "target": [
            "libremdb.iket.me",
            "libremdb.pussthecat.org",
            "ld.vern.cc",
            "binge.whatever.social",
            "libremdb.lunar.icu",
        ],
    },
    "translate": {
        "source": ["translate.google.com"],
        "target": [
            "lingva.ml",
            "translate.igna.wtf",
            "translate.plausibility.cloud",
            "translate.projectsegfau.lt",
            "translate.dr460nf1r3.org",
            "lingva.garudalinux.org",
            "translate.jae.fi",
        ],
    },
    "tiktok": {
        "source": ["tiktok.com"],
        "target": [
            "proxitok.pabloferreiro.es",
            "proxitok.pussthecat.org",
            "tok.habedieeh.re",
            "proxitok.privacydev.net",
            "proxitok.odyssey346.dev",
            "tok.artemislena.eu",
            "tok.adminforge.de",
            "proxitok.manasiwibi.com",
            "tik.hostux.net",
            "tt.vern.cc",
            "proxitok.mha.fi",
            "proxitok.pufe.org",
            "proxitok.marcopisco.com",
            "cringe.whatever.social",
            "proxitok.lunar.icu",
        ],
    },
    "imgur": {
        "source": ["imgur.com"],
        "target": [
            "imgur.artemislena.eu",
            "ri.zzls.xyz",
            "rimgo.bus-hit.me",
            "rimgo.fascinated.cc",
            "rimgo.hostux.net",
            "rimgo.kling.gg",
            "rimgo.lunar.icu",
            "rimgo.marcopisco.com",
            "rimgo.privacytools.io",
            "rimgo.projectsegfau.lt",
            "rimgo.pussthecat.org",
            "rimgo.totaldarkness.net",
            "rimgo.whateveritworks.org",
        ],
    },
    "medium": {
        "source": ["medium.com"],
        "target": [
            "scribe.rip",
            "scribe.nixnet.services",
            "scribe.citizen4.eu",
            "scribe.bus-hit.me",
            "scribe.froth.zone",
            "scribe.privacydev.net",
            "sc.vern.cc",
        ],
        "postprocess": fixScribePath
    },
    "google": {
        "source": ["google.com"],
        "target": [
            "whoogle.dcs0.hu",
        ],
    },
    "wiki-en": {
        "source": ["en.wikipedia.org"],
        "target": [
            "wiki.adminforge.de",
            "wiki.froth.zone",
            "wiki.slipfox.xyz",
            "wikiless.funami.tech",
            "wikiless.org",
            "wikiless.tiekoetter.com",
        ],
    },
    "wiki-de": {
        "source": ["de.wikipedia.org"],
        "target": [
            "wiki.adminforge.de",
        ],
    },
}


def rewrite(request: interceptor.Request):
    if (
        request.resource_type != interceptor.ResourceType.main_frame
        or request.request_url.scheme() in {"data", "blob"}
    ):
        return

    url = request.request_url

    for service in redirects.values():
        matched = False
        for source in service["source"]:
            if re.search(source, url.host()):
                matched = True

        if matched:
            target = service["target"][random.randint(0, len(service["target"]) - 1)]
            if target is not None and url.setHost(target) is not False:
                if "postprocess" in service:
                    service["postprocess"](url)
                try:
                    request.redirect(url)
                except RedirectException as e:
                    message.error(str(e))
                break


interceptor.register(rewrite)
