c.aliases["gem"] = "hint links userscript qute-gemini"

# Use q for quitting a tab (mimicks vim buffer) - qa is used for exiting
c.aliases["q"] = "tab-close"
# if we save sessions with w, load sessions with e (again, mimicks vim)
c.aliases["e"] = "session-load"

# wallabag add current page, either with walla command, or <leader>bw
c.aliases["add-wallabag"] = "spawn --userscript wallabag_add.sh"

# add to (my) shaarli instance
c.aliases["add-shaarli"] = "spawn --userscript shaarli_add.sh"

# re-opens the current page on the web archive overview page
c.aliases["send-to-archive"] = "open https://web.archive.org/web/{url}"

# save current page to pdf file
c.aliases["save-to-pdf"] = "spawn --userscript pagetopdf.sh"
# open sci-hub pdf for doi
c.aliases["send-to-scihub"] = "spawn --userscript doi2scihub"

# translate current page / selection with google translate
c.aliases["translate-page-google"] = "spawn --userscript translate_google.sh"
c.aliases[
    "translate-selection-google"
] = "spawn --userscript translate_google.sh --text"

# open the current page in more readable version
c.aliases["readable"] = "spawn --userscript readability"

# print picture of current url as qr code
c.aliases["show-qr"] = "spawn --userscript qr"

# add a task of current page to taskwarrior
c.aliases["taskadd"] = "spawn --userscript taskadd"

# try to remove the cookie banner from a website
c.aliases["cookie-block"] = "spawn --userscript qute-cookie-block"

# open last downloads in dmenu-like
c.aliases["recent-downloads"] = "spawn --userscript open_download"
