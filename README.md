# `~/🌹`

Note that the below screenshots still show the X configuration from [v0.1](https://gitlab.com/marty-oehme/dotfiles/-/tags/v0.1) which is very old by now.
The current dotfiles are geared toward wayland for which the setup looks similar but not identical to the previews below.

## What's in these dotfiles

* [x] vim configuration for simple programming tasks (especially go/typescript/python/bash) and prose
* [x] academic workflow tools, to allow quick citation, pdf compilation, and preview
* [x] simple, efficient waybar with package update notification, and spotify (mpris) integration
* [x] tmux session management through `tm` and `tl` tools
* [x] tmux fuzzy-searching of terminal sessions to switch to with hot-key (`<C-A><C-s>`) in addition to normal session switching
* [x] system-wide color management (terminals, vim, qutebrowser, polybar, xresources) through [`flavours`](https://github.com/Misterio77/flavours) application using [base16](http://chriskempson.com/projects/base16/) themes
* [x] quick theme switching by activating `flavours` and fuzzy-searching themes with hot-key (default `<Super>=<Shift>+S`)
* [x] many vim color-schemes with quick light/dark switching (`F8`) and individual theme switch (`<Space>+F8`)
* [x] quick directory jumping using `z`, with `fzf` integration
* [x] `fzf`-like integrations for bibtex citation, vim buffer management, most recently used switching, shell command history, and more

[![Styler recoloring demo](https://gitlab.com/marty-oehme/dotfiles/-/wikis/uploads/bde87deda694590a2e08e21552e11309/styler.webp)](https://gitlab.com/marty-oehme/dotfiles/-/wikis/uploads/90894e53eff378db4d7f9f49e7a69fab/styler.mp4)

## Quick-Start

The dotfiles use `GNU stow` to link themselves in the home directory. You can clone this repository anywhere (though I have mine in `~/.dotfiles` as it seemed most logical for me).

I would recommend doing an initial `git clone --recursive` for this repository, since it contains git [submodules](https://nering.dev/2016/git-submodules-vs-subtrees/), which will then automatically get pulled in as well.
Of course, you can do it non-recursively and then just pull those modules selectively which you actually want.

Once in the repository directory, when you then run `./install.sh` it will install many of the packages I use (though they are probably slightly out-of-date) and link the dotfiles into the home directory.
I would mostly recommend this on fresh machines or a test machine first - it *will* link my personal dotfiles and, if you allow it, *will* install quite a few packages.
By default it will ask your consent for some steps -- use `./install.sh -f` to force yes to everything.

The dotfile installation procedure is based on `stow`, it will *not overwrite* anything already in the home directory (though you can force it to if you really want, using `stow --override='.*'` -- I do not recommend this).

> **NOTE**
> The same non-destructive installation procedure does *not* apply to the package installation and system setting file linking, where it can potentially overwrite or remove existing files.

After all files are linked and you open a new shell session, the `dotlink` alias will allow you to re-link all dotfiles from anywhere on the system.[^1]

[^1]: This alias only works when the dotfiles are cloned into `~/.dotfiles`, mirroring my setup.
    This is due to a hard-coded cd into this directory.
    If your dotfiles lie in another directory and you want to use the dotlink alias, simply change the corresponding line in `bootstrap/.config/sh/alias.d/dotlink.sh`]

Both automatic installation paths are presumably somewhat brittle. In any case, I would suggest to manually look through the files for things you want instead of copying and activating everything.
Dotfiles are too personal to be standardized like that.
They're pets, not cattle.
Enjoy!

## Main Modules

![Overview - an older image of the dotfile desktop with gaps, showing git logs, styler logs, duckduckgo in a browser, and a vifm view of the dotfiles themselves](https://gitlab.com/marty-oehme/dotfiles/-/wikis/uploads/aaf0319d575dc192ea0f4bd6eaf83c08/gaps.png)

* [`wayland`](https://github.com/wayland-project/wayland) - Containing basics for fully functional tiling wayland setup:
    * [`river`](https://github.com/riverwm/river) - Tiling window manager for wayland
    * [`waybar`](https://github.com/Alexays/Waybar) - Easily customizable statusbar for wayland
    * [`bemenu`](https://github.com/Cloudef/bemenu) - Extended dmenu replacement for wayland, X11 and ncurses
    * [`fontconfig`] - System-wide font replacements and styling settings
* [`wezterm`](https://wezfurlong.org/wezterm/) - Terminal emulator (fast, understandable and lua configurable)
* [`tmux`](https://github.com/tmux/tmux/) - terminal multiplexer (slowly migrating away in favor of wezterm)
* [`nvim`](https://neovim.io/) - Neovim configuration
* [`vifm`](https://github.com/vifm/vifm) - vim-like file-manager
* [`qutebrowser`](https://github.com/qutebrowser/qutebrowser) - vim-key enabled web browser
* [`pass`](pass/README.md) - Password management suite
* [`bibtex`](bibtex/README.md) - LateX/BibteX/pandoc plaintext writing & reference suite
* [`git`](git/README.md) - distributed version control system.
* [`office`](office/README.md) - office/productivity software for writing e-mail and setting appointments

## Notes

* Generally, most configuration for applications attempts to follow the XDG specifications, keeping configuration in .config directory and supplementary files in .local/share directory. Over time, I am moving more applications to this standard: it keeps the home directory clean, and the separation of configuration, binaries, and data relatively clear.
* The `zsh` directory contains all setup for the z-shell, my daily work environment. It should not be required for working with any other module but will add additional functionality to many (such as command auto-completion and so on). `sh` sets some base functionality for any shell you may wish to work in. It is, for now, the only module that is required for some other modules to work.[^shreq]
* `rofi` contains additional scripts and a simple theming framework for rofi and should probably be reorganized to put the correct files into the correct directories (per xdg) at some point.
* Whereas `sh` module scripts are requirements for other scripts, `.local/bin` in the `scripts` module contains most executable user scripts. Most of these have been migrated to other corresponding modules (e.g. if a script exclusively targets git functionality, it will live there), some useful --- or left-over --- stand-alone scripts remain however.
* `.local/share/pandoc` contains configuration for academic latex writing (pandoc, really) and is of interest if you want to use this functionality.
* `.xinitrc` is used for x initialization and program startup. At some point, some of the consistently running applications may be moved to systemd/runit as supervised services.
* Generally, top-level directories starting with a . are only meaningful for the *repository* not for the functionality of the machine that these dotfiles are deployed on. That means `.gitlab-ci.yml`, `.assets/`, `.stowrc` and similar files and directories will not show up in the final deployment in any home directory. Perhaps they should be called dotdot-files since they're the dotfiles for my dotfiles. 🙂 (Also, '[dotfiles](https://en.wikipedia.org/wiki/Semantic_satiation)'.)

[^shreq]: I may remove this requirement in the future to make modules more self-contained. However, relying on some base utility scripts makes it easier to avoid duplicating such functionality for each individual script in other modules.

![Gapless - the same image as above, only displayed without gaps](https://gitlab.com/marty-oehme/dotfiles/-/wikis/uploads/21791f77da013cdac64f11eff61584e3/gapless.png)
