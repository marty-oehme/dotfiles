-- many ideas for this config come from
-- https://github.com/elianiva/dotfiles/ - with much gratitude
local api = vim.api

api.nvim_exec2("runtime abbrev.vim", {})

for _, source in ipairs({
	"core.settings",
	"core.lazy",
	"core.autocmds",
	"core.mappings",
	"core.look",
	"personal",
}) do
	local status_ok, fault = pcall(require, source)
	if not status_ok then
		vim.api.nvim_err_writeln("Failed to load " .. source .. "\n\n" .. fault)
	end
end

-- to include e.g. the spell dictionaries for vim
vim.opt.rtp:append(vim.fn.stdpath("data") .. "/site")
