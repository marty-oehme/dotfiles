local ns = vim.api.nvim_create_namespace("gitcommit")
vim.api.nvim_set_hl(ns, "ColorColumn", { bg = "#a33a3a", blend = 90 })
vim.api.nvim_win_set_hl_ns(0, ns)
vim.bo.textwidth = 72
vim.wo.colorcolumn = "+0"
