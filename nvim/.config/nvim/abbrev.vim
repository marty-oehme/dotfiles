" Typos
iabbrev adn and
iabbrev waht what
iabbrev tehn then
iabbrev whit with
iabbrev whith with
iabbrev grwoth growth
iabbrev Grwoth Growth
iabbrev teh the
iabbrev projcets projects

" Text expansion
iabbrev mo@ marty.oehme@gmail.com
iabbrev mome@ <https://martyoeh.me/>
