return {
	{ "edKotinsky/Arduino.nvim", ft = "arduino", config = true }, -- statusline
	{ "euclidianAce/BetterLua.vim", ft = "lua" }, -- better syntax highlighting for lua
	{ "aliou/bats.vim", ft = { "bash", "sh", "zsh", "bats" } }, -- enable syntax for bats shell-code testing library
}
