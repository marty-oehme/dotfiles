return {
	{
		"lewis6991/gitsigns.nvim", -- show vcs changes on left-hand gutter
		event = "VeryLazy",
		config = function()
			require("gitsigns").setup({
				numhl = true,
				signcolumn = false,
				on_attach = function(bufnr)
					local gs = package.loaded.gitsigns

					local function map(mode, l, r, opts)
						opts = opts or {}
						opts.buffer = bufnr
						vim.keymap.set(mode, l, r, opts)
					end

					-- Navigation
					map("n", "]h", function()
						if vim.wo.diff then
							return "]h"
						end
						vim.schedule(function()
							gs.next_hunk()
						end)
						return "<Ignore>"
					end, { expr = true })

					map("n", "[h", function()
						if vim.wo.diff then
							return "[h"
						end
						vim.schedule(function()
							gs.prev_hunk()
						end)
						return "<Ignore>"
					end, { expr = true })

					-- Actions
					require("which-key").register({ ["<localleader>h"] = { name = "+git" } })
					map({ "n", "v" }, "<localleader>hs", ":Gitsigns stage_hunk<CR>", { desc = "stage hunk" })
					map({ "n", "v" }, "<localleader>hr", ":Gitsigns reset_hunk<CR>", { desc = "reset hunk" })
					map("n", "<localleader>hS", gs.stage_buffer, { desc = "stage buffer" })
					map("n", "<localleader>hu", gs.undo_stage_hunk, { desc = "undo stage hunk" })
					map("n", "<localleader>hR", gs.reset_buffer, { desc = "reset buffer" })
					map("n", "<localleader>hp", gs.preview_hunk, { desc = "preview hunk" })
					map("n", "<localleader>hb", function()
						gs.blame_line({ full = true })
					end, { desc = "blame line" })
					map("n", "<localleader>hB", gs.toggle_current_line_blame, { desc = "toggle blame" })
					map("n", "<localleader>hd", gs.diffthis, { desc = "diffthis" })
					map("n", "<localleader>hD", function()
						gs.diffthis("~")
					end, { desc = "diffbase" })
					map("n", "<localleader>ht", gs.toggle_deleted, { desc = "toggle deleted" })

					-- Text object
					map({ "o", "x" }, "ih", ":<C-U>Gitsigns select_hunk<CR>")
					map({ "o", "x" }, "ah", ":<C-U>Gitsigns select_hunk<CR>")
				end,
			})
		end,
	},
}
