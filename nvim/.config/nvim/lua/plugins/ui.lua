return {
	-- statusline
	{
		"nvim-lualine/lualine.nvim",
		requires = { "nvim-tree/nvim-web-devicons", config = true },
		config = function()
			require("plugins.config.lualine")
		end,
		event = "VeryLazy",
	},
	-- create a pretty pop-up notification
	{
		"rcarriga/nvim-notify",
		config = function()
			vim.notify = require("notify")
		end,
		event = "VeryLazy",
	},
	-- make all vim.ui interfaces prettyy
	{ "stevearc/dressing.nvim", config = true, event = "VeryLazy" },

	-- numbers to absolute for all buffers but the current which is relative
	{ "jeffkreeftmeijer/vim-numbertoggle", event = "VeryLazy" },
	-- auto-hiding colorcolumn
	{
		"m4xshen/smartcolumn.nvim",
		event = "VeryLazy",
		opts = {
			colorcolumn = { "100" },
			scope = "window",
			disabled_filetypes = {
				"help",
				"text",
				"markdown",
				"NvimTree",
				"lazy",
				"mason",
				"help",
				"quarto",
			},
		},
	},
	-- display pretty colors when they are mentioned in buffer
	{
		"NvChad/nvim-colorizer.lua", -- color hex, named colors in the correct preview scheme
		config = function()
			require("colorizer").setup({
				user_default_options = { mode = "virtualtext" },
			})
		end,
		event = "VeryLazy",
	},
	{
		"jiaoshijie/undotree",
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		config = true,		event = "VeryLazy",
	},
}
