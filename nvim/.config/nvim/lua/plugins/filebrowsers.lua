return {
	{
		"vifm/vifm.vim",
		config = function()
			vim.g.loaded_netrw = 1
			vim.g.loaded_netrwPlugin = 1
			vim.g.vifm_replace_netrw = 1
			vim.g.vifm_exec_args = '-c "set vifminfo=" -c "set statusline=" -c "only"'
		end,
		cmd = "Vifm",
	}, -- integrate file manager
	{
		"nvim-tree/nvim-tree.lua", -- integrate file tree
		config = true,
		dependencies = { "nvim-tree/nvim-web-devicons", config = true },
		cmd = "NvimTreeToggle",
	},
}
