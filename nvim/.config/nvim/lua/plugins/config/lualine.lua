require("lualine").setup({
	options = {
		icons_enabled = true,
		theme = "auto",
		component_separators = { left = "", right = "" },
		section_separators = { left = "", right = "" },
		disabled_filetypes = {},
		always_divide_middle = true,
	},
	sections = {
		lualine_a = { "mode" },
		lualine_b = { "branch", "diff", "diagnostics" },
		lualine_c = { "filename" },
		lualine_x = { "encoding", "fileformat", "filetype" },
		lualine_y = { "progress", "location" },
		lualine_z = { "hostname" },
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = { "branch", "diff" },
		lualine_c = { "filename" },
		lualine_x = {},
		lualine_y = { "location" },
		lualine_z = {},
	},
	tabline = {},
	extensions = { "quickfix", "toggleterm" },
})
