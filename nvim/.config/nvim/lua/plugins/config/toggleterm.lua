require("toggleterm").setup({
	open_mapping = [[<leader>=]],
	insert_mappings = false, -- don't map the key in insert mode
	terminal_mappings = false,
})

local Terminal = require("toggleterm.terminal").Terminal

-- need to disable indentlines since they obscure first line of terminal
if require("util").is_available("mini.nvim") then
	vim.api.nvim_create_autocmd({ "TermOpen" }, {
		pattern = "*",
		callback = function()
			vim.b.miniindentscope_disable = true
		end,
	})
end

local function custom_term_set_toggle_key(term)
	vim.keymap.set("t", "<C-\\>", function()
		term:toggle()
	end, { silent = true, buffer = true })
end

-- create python window
local function get_python_cmd()
	if vim.fn.executable("ptipython") then
		return "ptipython"
	end
	if vim.fn.executable("ipython") then
		return "ipython"
	end
	if vim.fn.executable("ptpython") then
		return "ptpython"
	end
	if vim.fn.executable("python") then
		return "python"
	end
end
local terms = {
	lazygit = Terminal:new({
		cmd = "lazygit",
		hidden = true,
		direction = "float",
		float_opts = { border = "curved" },
		on_open = custom_term_set_toggle_key,
	}),
	python = Terminal:new({
		cmd = get_python_cmd(),
		hidden = true,
		direction = "float",
		float_opts = { border = "curved" },
		on_open = custom_term_set_toggle_key,
	}),
}
-- create a lazygit window with the lazygit command
local function toggle_custom_term(term, bang, vertsize)
	vertsize = vertsize or vim.o.columns * 0.4
	if not bang then
		term.direction = "float"
		term:toggle()
	else
		term.direction = "vertical"
		term:resize(vertsize)
		term:toggle()
	end
end

local function _Pythonterm_toggle(opts)
	toggle_custom_term(terms.python, opts.bang)
end
local function _Lazygit_toggle(opts)
	toggle_custom_term(terms.lazygit, opts.bang, vim.o.columns * 0.6)
end

vim.api.nvim_create_user_command(
    "Lazygit",
    _Lazygit_toggle,
    { desc = "Toggle floating Lazygit terminal", bang = true }
)
vim.api.nvim_create_user_command(
	"Pythonterm",
	_Pythonterm_toggle,
	{ desc = "Toggle floating Python terminal", bang = true }
)
