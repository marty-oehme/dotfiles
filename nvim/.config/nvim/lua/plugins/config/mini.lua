require("mini.ai").setup()
require("mini.comment").setup({
	hooks = {
		pre = function()
			require("ts_context_commentstring.internal").update_commentstring()
		end,
	},
})
require("mini.cursorword").setup({ delay = 500 })
require("mini.fuzzy").setup()
require("mini.indentscope").setup({
	symbol = "│",
	draw = { animation = require("mini.indentscope").gen_animation.none() },
	options = { indent_at_cursor = false },
})
require("mini.map").setup()
require("mini.move").setup() -- has not hit stable yet
require("mini.pairs").setup()
require("mini.trailspace").setup()

local starter = require("mini.starter")
starter.setup({
	evaluate_single = true,
	items = {
		starter.sections.builtin_actions(),
		starter.sections.recent_files(10, false),
		starter.sections.recent_files(10, true),
		-- Use this if you set up 'mini.sessions'
		starter.sections.telescope(),
	},
	content_hooks = {
		starter.gen_hook.adding_bullet(),
		starter.gen_hook.padding(3, 2),
		starter.gen_hook.aligning("center", "center"),
	},
})

vim.api.nvim_set_hl(0, "MiniCursorword", { bold = true })
