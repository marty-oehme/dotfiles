local luasnip = require("luasnip")
local cmp = require("cmp")

local has_words_before = function()
	local line, col = unpack(vim.api.nvim_win_get_cursor(0))
	return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local kind_icons = {
	Text = "",
	Method = "",
	Function = "󰊕",
	Constructor = "",
	Field = "",
	Variable = "",
	Class = "",
	Interface = "",
	Module = "",
	Property = "",
	Unit = "",
	Value = "V",
	Enum = "",
	Keyword = "",
	Snippet = "",
	Color = "",
	File = "",
	Reference = "",
	Folder = "",
	EnumMember = "",
	Constant = "",
	Struct = "",
	Event = "",
	Operator = "",
	TypeParameter = "",
}

cmp.setup({
	window = { documentation = cmp.config.window.bordered() },
	snippet = {
		expand = function(args)
			require("luasnip").lsp_expand(args.body)
		end,
	},
	sources = {
		{ name = "nvim_lsp" },
		{ name = "otter" },
		{ name = "luasnip", keyword_length = 2 },
		{ name = "pandoc_references" },
		{ name = "nvim_lua" },
		{
			name = "beancount",
			option = {
				account = vim.env["HOME"] .. "/documents/records/budget/main.beancount", -- TODO implement dynamically
			},
		},
		{ name = "calc" },
		{ name = "path" },
		{ name = "buffer", keyword_length = 3 },
		{ name = "digraphs" },
		{ name = "latex_symbols" },
		{ name = "spell", keyword_length = 3 },
		{ name = "tmux" }, -- { name = 'rg',           keyword_length = 5 },
		{ name = "vCard" },
	},
	mapping = cmp.mapping.preset.insert({
		["<C-b>"] = cmp.mapping.scroll_docs(-4),
		["<C-f>"] = cmp.mapping.scroll_docs(4),
		["<CR>"] = cmp.mapping({
			i = function(fallback)
				if cmp.visible() and cmp.get_active_entry() then
					cmp.confirm({
						behavior = cmp.ConfirmBehavior.Replace,
						select = false,
					})
				else
					fallback()
				end
			end,
			s = cmp.mapping.confirm({ select = true }),
			c = cmp.mapping.confirm({
				behavior = cmp.ConfirmBehavior.Replace,
				select = false,
			}), -- disable selection in cmd mode
		}),
		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
				-- You could replace the expand_or_jumpable() calls with expand_or_locally_jumpable()
				-- they way you will only jump inside the snippet region
			elseif luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			elseif has_words_before() then
				cmp.complete()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<S-Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
	}),
	formatting = {
		fields = { "kind", "abbr", "menu" },
		format = function(entry, vim_item)
			-- Kind icons, removing kind text leaving only icon
			-- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind)
			vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
			-- Source
			vim_item.menu = ({
				buffer = "[Buf]",
				calc = "[Cal]",
				digraphs = "[Dig]",
				latex_symbols = "[LaTeX]",
				luasnip = "[Snip]",
				nvim_lsp = "[Lsp]",
				nvim_lua = "[Lua]",
				pandoc_references = "[Bib]",
				spell = "[Spl]",
				vCard = "[vCrd]",
			})[entry.source.name]
			return vim_item
		end,
	},
})
-- `/` cmdline setup.
cmp.setup.cmdline("/", {
	mapping = cmp.mapping.preset.cmdline(),
	sources = { { name = "buffer" } },
})
-- `:` cmdline setup.
cmp.setup.cmdline(":", {
	mapping = cmp.mapping.preset.cmdline(),
	sources = cmp.config.sources({ { name = "path" } }, {
		{ name = "cmdline", option = { ignore_cmds = { "Man", "!" } } },
	}),
})
