vim.diagnostic.config({ virtual_text = true })
vim.fn.sign_define("DiagnosticSignError", { text = "✘", texthl = "DiagnosticSignError" })
vim.fn.sign_define("DiagnosticSignWarn", { text = "", texthl = "DiagnosticSignWarn" })
vim.fn.sign_define("DiagnosticSignInfo", { text = "", texthl = "DiagnosticSignInfo" })
vim.fn.sign_define("DiagnosticSignHint", { text = "", texthl = "DiagnosticSignHint" })

local lsp = require("lsp-setup")

local servers = {
	ansiblels = {},
	arduino_language_server = {},
	astro = {},
	bashls = {},
	beancount = {},
	clangd = {},
	cssls = {},
	docker_compose_language_service = {},
	dockerls = {},
	emmet_ls = {},
	gopls = {},
	julials = {},
	lua_ls = {
		settings = {
			Lua = {
				diagnostics = { globals = { "vim" } },
				-- enable when working on neovim stuff. Takes *long* to load
				-- workspace = { library = vim.api.nvim_get_runtime_file("", true) },
				telemetry = { enable = false },
			},
		},
	},
	marksman = {},
	pyright = {},
	ruff_lsp = {},
	tailwindcss = {},
	taplo = {},
	texlab = {},
	tsserver = {},
	yamlls = {},
}

local function on_attach(client, bufnr)
	local map = vim.keymap.set
	map("n", "[d", "<cmd>lua vim.diagnostic.goto_prev()<cr>", { buffer = bufnr, desc = "Previous diagnostic" })
	map("n", "]d", "<cmd>lua vim.diagnostic.goto_next()<cr>", { buffer = bufnr, desc = "Next diagnostic" })
	map(
		"n",
		"[e",
		"<cmd>lua vim.diagnostic.goto_prev({severity = vim.diagnostic.severity.ERROR})<cr>",
		{ buffer = bufnr, desc = "Previous error" }
	)
	map(
		"n",
		"]e",
		"<cmd>lua vim.diagnostic.goto_next({severity = vim.diagnostic.severity.ERROR})<cr>",
		{ buffer = bufnr, desc = "Next error" }
	)

	local prefix = require("which-key").register
	prefix({ ["<localleader>l"] = { name = "+lsp" } })
	map("n", "<localleader>li", "<cmd>LspInfo<cr>", { buffer = bufnr, desc = "Lsp Info" })
	map(
		"n",
		"<localleader>ld",
		"<cmd>lua vim.diagnostic.open_float()<cr>",
		{ buffer = bufnr, desc = "Line diagnostics" }
	)
	map("n", "<localleader>la", "<cmd>lua vim.lsp.buf.code_action()<cr>", { buffer = bufnr, desc = "Codeactions" })
	map("n", "<localleader>ln", "<cmd>lua vim.lsp.buf.rename()<cr>", { buffer = bufnr, desc = "Rename element" })
	if vim.fn.exists(":Telescope") then
		map("n", "<localleader>lr", "<cmd>Telescope lsp_references<cr>", { buffer = bufnr, desc = "References" })
		map("n", "<localleader>lf", "<cmd>Telescope lsp_definitions<cr>", { buffer = bufnr, desc = "Definition" })
		map(
			"n",
			"<localleader>lt",
			"<cmd>Telescope lsp_type_definitions<cr>",
			{ buffer = bufnr, desc = "Type definition" }
		)
		map(
			"n",
			"<localleader>lm",
			"<cmd>Telescope lsp_implementations<cr>",
			{ buffer = bufnr, desc = "Implementation" }
		)
	else
		map("n", "<localleader>lr", "<cmd>lua vim.lsp.buf.references()<cr>", { buffer = bufnr, desc = "References" })
		map("n", "<localleader>lf", "<cmd>lua vim.lsp.buf.definition()<cr>", { buffer = bufnr, desc = "Definition" })
		map(
			"n",
			"<localleader>lt",
			"<cmd>lua vim.lsp.buf.type_definition()<cr>",
			{ buffer = bufnr, desc = "Type definition" }
		)
		map(
			"n",
			"<localleader>lm",
			"<cmd>lua vim.lsp.buf.implementation()<cr>",
			{ buffer = bufnr, desc = "Implementation" }
		)
	end
	map("n", "<localleader>ll", "<cmd>lua vim.lsp.buf.format()<CR>", { buffer = bufnr, desc = "Format document" })
	map("n", "K", "<cmd>lua vim.lsp.buf.hover()<cr>", { buffer = bufnr, desc = "Hover definition" })
	map("n", "<localleader>lc", "<cmd>lua vim.lsp.buf.declaration()<cr>", { buffer = bufnr, desc = "Declaration" })
	map(
		"n",
		"<localleader>ls",
		"<cmd>lua vim.lsp.buf.signature_help()<cr>",
		{ buffer = bufnr, desc = "Signature help" }
	)

	if vim.g.format_on_save then
		require("lsp-setup.utils").format_on_save(client)
	end
end

lsp.setup({
	default_mappings = false,
	servers = servers,
	on_attach = on_attach,
	inlay_hints = {
		enabled = vim.fn.has("nvim-0.10") == true and true or false,
	},
})

local lspconfig = require("lspconfig")

-- ensure python virtualenv is determined automatically on lsp start
lspconfig.pyright.setup({
	on_attach = function(client, bufnr)
		on_attach(client, bufnr)
		local python_path, _ = require("util").get_python_venv(client.config.root_dir)
		vim.notify_once(string.format("[PYTHON VENV]\n%s", python_path))
		client.config.settings.python.pythonPath = python_path
	end,
})
-- set up arduino with the help of arduino.nvim plugin
if require("util").is_available("arduino") then
	lspconfig.arduino_language_server.setup({
		on_new_config = require("arduino").on_new_config,
	})
end

local null_ls = require("null-ls")
null_ls.setup({})
require("mason-null-ls").setup({
	ensure_installed = { "black", "prettier", "shfmt", "eslint-lsp", "stylua", "jq", "vale" },
	automatic_installation = false,
	handlers = {
		shfmt = function(_, _)
			null_ls.register(null_ls.builtins.formatting.shfmt.with({
				extra_filetypes = { "bash", "zsh" },
			}))
		end,
		prettier = function(_, _)
			null_ls.register(null_ls.builtins.formatting.prettier.with({
				extra_filetypes = { "astro" },
				timeout = 7000,
			}))
		end,
		eslint = function(_, _)
			null_ls.register(null_ls.builtins.diagnostics.eslint.with({
				extra_filetypes = { "astro" },
			}))
			null_ls.register(null_ls.builtins.code_actions.eslint.with({
				extra_filetypes = { "astro" },
			}))
		end,
		vale = function(_, _)
			null_ls.register(null_ls.builtins.diagnostics.vale.with({
				condition = function(utils)
					return (utils.root_has_file({ ".vale.ini", "_vale.ini" }) and utils.root_has_file({ "styles/" }))
				end,
			}))
		end,
	},
})
