local writing_ft = { "quarto", "pandoc", "markdown", "text", "tex" }

return {
	-- UI improvements
	-- provide distraction free writing
	{ "folke/zen-mode.nvim", config = true, event = "VeryLazy" },
	-- provide even distraction free-er writing (lowlight paragraphs)
	{ "folke/twilight.nvim", event = "VeryLazy" },
	-- enable 'speed-reading' mode (bionic reading)
	{
		"JellyApple102/easyread.nvim",
		config = true,
		ft = writing_ft,
		cmd = "EasyreadToggle",
	},
	{
		"andrewferrier/wrapping.nvim",
		config = function()
			require("wrapping").setup({
				create_keymappings = false,
				notify_on_switch = false,
			})
		end,
	},
	-- generate an auto-updating html preview for md files
	{
		"iamcco/markdown-preview.nvim",
		build = function()
			vim.fn["mkdp#util#install"]()
		end,
		ft = writing_ft,
	},

	-- bring zettelkasten commands
	{
		"mickael-menu/zk-nvim",
		config = function()
			require("zk").setup({ picker = "telescope" })
		end,
		ft = writing_ft,
		cmd = {
			"ZkBacklinks",
			"ZkCd",
			"ZkIndex",
			"ZkInsertLink",
			"ZkInsertLinkAtSelection",
			"ZkLinks",
			"ZkMatch",
			"ZkNew",
			"ZkNewFromContentSelection",
			"ZkNewFromTitleSelection",
			"ZkNotes",
			"ZkTags",
		},
	},
	-- simple static markdown linking and link following using zettel IDs
	{ "marty-oehme/zettelkasten.nvim", ft = writing_ft, event = "VeryLazy" },

	-- syntax highlighting for markdown criticmarkup (comments, additions, ...)
	{ "vim-pandoc/vim-criticmarkup", ft = writing_ft },
	-- inline display of latex formulas
	-- TODO always demands latex treesitter to be installed even if it is
	-- TODO always turns softwrapped lines off on exiting insert mode
	--{
	--"jbyuki/nabla.nvim",
	--ft = writing_ft,
	--config = function()
	--require("nabla").enable_virt({ autogen = true, silent = true })
	--end,
	--},
}
