return {
	{
		"quarto-dev/quarto-nvim",
		dependencies = {
			"jmbuhr/otter.nvim",
			"neovim/nvim-lspconfig",
			"vim-pandoc/vim-pandoc-syntax",
			"hrsh7th/nvim-cmp",
			"nvim-treesitter/nvim-treesitter",
		},
		config = function()
			require("quarto").setup({
				lspFeatures = {
					enabled = true,
					languages = { "r", "python", "julia" },
					diagnostics = { enabled = true, triggers = { "BufWrite" } },
					completion = { enabled = true },
				},
			})
		end,
		lazy = false,
		ft = "quarto",
	},

	{
		"lkhphuc/jupyter-kernel.nvim",
		config = true,
		cmd = "JupyterAttach",
		build = ":UpdateRemotePlugins",
		keys = {
			{
				"<localleader>ck",
				"<Cmd>JupyterInspect<CR>",
				desc = "Inspect object in kernel",
			},
		},
		lazy = false,
	},

	-- REPL work
	{
		"WhiteBlackGoose/magma-nvim-goose",
		build = ":UpdateRemotePlugins",
		config = function()
			vim.g.magma_image_provider = "kitty"
			vim.g.magma_automatically_open_output = false
		end,
		cmd = {
			"MagmaInit",
			"MagmaEvaluateOperator",
			"MagmaEvaluateLine",
			"MagmaEvaluateVisual",
			"MagmaRestart",
		},
		ft = { "quarto", "python" },
		lazy = false,
	},
}
