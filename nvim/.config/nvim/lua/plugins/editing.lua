return {
	-- adds exchange operator with cx. common use: cxiw . on 2 words to switch
	{
		"tommcdo/vim-exchange",
		event = "VeryLazy",
	},
	-- Align tables and other alignable things
	{
		"junegunn/vim-easy-align",
		event = "VeryLazy",
	},
	-- surround things with other things using ys/cs/ds
	{ "kylechui/nvim-surround", version = "*", config = true, event = "VeryLazy" },

	-- extend the ^a / ^x possibilities to dates, hex, alphabets, markdown headers
	{
		"monaqa/dial.nvim",
		config = function()
			local augend = require("dial.augend")
			require("dial.config").augends:register_group({
				-- default augends used when no group name is specified
				default = {
					augend.integer.alias.decimal,
					augend.integer.alias.hex,
					augend.date.alias["%Y/%m/%d"],
					augend.date.alias["%Y-%m-%d"],
					augend.date.alias["%m/%d"],
					augend.date.alias["%H:%M:%S"],
					augend.date.alias["%H:%M"],
					augend.constant.alias.de_weekday_full,
					augend.constant.alias.de_weekday,
					augend.constant.alias.bool,
					augend.semver.alias.semver,
					augend.constant.alias.Alpha,
					augend.constant.alias.alpha,
					augend.hexcolor.new({ case = "lower" }),
					augend.constant.new({
						elements = { "and", "or" },
						word = true, -- if false, "sand" is incremented into "sor", "doctor" into "doctand", etc.
						cyclic = true, -- "or" is incremented into "and".
					}),
					augend.constant.new({
						elements = { "&&", "||" },
						word = false,
						cyclic = true,
					}),
				},
			})
		end,
		event = "VeryLazy",
	},
}
