return {
	-- fuzzy matching
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-lua/popup.nvim",
			"nvim-lua/plenary.nvim",
			{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
		},
		event = "VeryLazy",
		cmd = "Telescope",
		config = function()
			-- Setup up telescope fuzzy finding settings
			--
			-- Makes use of optionally installed external programs to work fully:
			-- rg (ripgrep) for in-text searches
			-- fd for quicker directory structure searches
			-- lsp for a variety of lsp queries
			require("telescope").setup({
				defaults = {
					vimgrep_arguments = {
						"rg",
						"--ignore-vcs",
						"--hidden",
						"--color=never",
						"--no-heading",
						"--with-filename",
						"--line-number",
						"--column",
						"--smart-case",
					},
					generic_sorter = require("mini.fuzzy").get_telescope_sorter,
					-- Appearance
					prompt_prefix = "  ",
					selection_caret = "󰳟 ",
					color_devicons = true,
				},
				pickers = {
					buffers = { theme = "ivy" },
					oldfiles = { theme = "ivy" },
					find_files = {
						theme = "dropdown",
						-- nice minimal picker design
						borderchars = {
							{ "─", "│", "─", "│", "┌", "┐", "┘", "└" },
							prompt = { "─", "│", " ", "│", "┌", "┐", "│", "│" },
							results = {
								"─",
								"│",
								"─",
								"│",
								"├",
								"┤",
								"┘",
								"└",
							},
							preview = {
								"─",
								"│",
								"─",
								"│",
								"┌",
								"┐",
								"┘",
								"└",
							},
						},
						width = 0.8,
						previewer = false,
						prompt_title = false,
					},
				},
			})
			require("telescope").load_extension("fzf")
		end,
	},
}
