return {
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		config = function()
			local rainbow = require("ts-rainbow")
			require("nvim-treesitter.configs").setup({
				-- one of "all", "maintained" (parsers with maintainers), or a list of languages
				ensure_installed = "all",
				highlight = {
					enable = true,
					disable = function(_, bufnr)
						return vim.api.nvim_buf_line_count(bufnr) > 7000
					end,
				},
				incremental_selection = { enable = true },
				textobjects = { enable = true },
				indent = { enable = true },
				autotag = { enable = true },

				-- enable rainbow brackets, needs p00f/nvim-ts-rainbow
				rainbow = {
					enable = true,
					strategy = { rainbow.strategy.global },
				},

				-- for improved commentstrings, needs corresponding plugin
				context_commentstring = {
					enable = true,
					enable_autocmd = false, -- since we run it as a hook from the mini.comment plugin
				},
				-- allows using . and ; to target treesitter branches
				textsubjects = {
					enable = true,
					keymaps = {
						["."] = "textsubjects-smart",
						[";"] = "textsubjects-container-outer",
					},
				},
			})
		end,
		event = "BufReadPost",
		cmd = {
			"TSBufDisable",
			"TSBufEnable",
			"TSBufToggle",
			"TSDisable",
			"TSEnable",
			"TSToggle",
			"TSInstall",
			"TSInstallInfo",
			"TSInstallSync",
			"TSModuleInfo",
			"TSUninstall",
			"TSUpdate",
			"TSUpdateSync",
		},

		-- rainbow brackets using treesitter
		-- show current cursor context at top of buffer
		-- improves commenting plugin above by using ts
		dependencies = {
			"https://gitlab.com/HiPhish/nvim-ts-rainbow2.git",
			{ "romgrk/nvim-treesitter-context", config = true },
			"JoosepAlviste/nvim-ts-context-commentstring",
			{
				"RRethy/nvim-treesitter-textsubjects",
			},
			"windwp/nvim-ts-autotag",
		},
	},
	{ "nvim-treesitter/playground", cmd = "TSPlaygroundToggle" }, -- interactively view and query the treesitter tree
}
