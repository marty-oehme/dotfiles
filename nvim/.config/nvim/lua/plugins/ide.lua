return {
	-- vista-like outline view for code
	{ "simrat39/symbols-outline.nvim", config = true, cmd = "SymbolsOutline" },
	-- show a signature whenever editing a function or similar
	{ "ray-x/lsp_signature.nvim", config = true, event = "VeryLazy" },
	{
		"junnplus/lsp-setup.nvim",
		dependencies = {
			{
				"neovim/nvim-lspconfig",
				-- will sometimes not keep up with lsp changes if set to stable
				version = false,
			},
			{
				"williamboman/mason.nvim",
				cmd = {
					"Mason",
					"MasonInstall",
					"MasonUninstall",
					"MasonUninstallAll",
					"MasonLog",
					"MasonUpdate",
				},
				build = ":MasonUpdate",
			},
			{
				"williamboman/mason-lspconfig.nvim",
				cmd = { "LspInstall", "LspUninstall" },
			},
			{
				"jose-elias-alvarez/null-ls.nvim",
				dependencies = {
					"nvim-lua/plenary.nvim",
					"jay-babu/mason-null-ls.nvim",
				},
				event = "VeryLazy",
			},
		},
		event = "BufReadPost",
		config = function()
			require("plugins.config.lsp")
		end,
	},
	{
		"hrsh7th/nvim-cmp",
		branch = "main",
		dependencies = {
			"andersevenrud/cmp-tmux",
			"cbarrete/completion-vcard",
			"f3fora/cmp-spell",
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-calc",
			"hrsh7th/cmp-cmdline",
			"hrsh7th/cmp-nvim-lua",
			"dmitmel/cmp-digraphs",
			"jc-doyle/cmp-pandoc-references",
			"kdheepak/cmp-latex-symbols",
			"lukas-reineke/cmp-rg",
			"crispgm/cmp-beancount",
			"ray-x/cmp-treesitter",
			"saadparwaiz1/cmp_luasnip",
			{
				"L3MON4D3/LuaSnip",
				dependencies = { "rafamadriz/friendly-snippets" },
			},
		},
		config = function()
			require("plugins.config.cmp")
		end,
		event = { "InsertEnter", "CmdlineEnter", "VeryLazy" },
	},
	-- loading animations for some LSP
	{ "j-hui/fidget.nvim", config = true, tag = "legacy", event = "VeryLazy" },
	-- a pretend-lsp for formatters and linters
}
