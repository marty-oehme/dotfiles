return {
	-- allow seamless navigation between vim buffers and tmux/wezterm splits
	{
		"numToStr/Navigator.nvim",
		branch = "master",
		config = true,
		event = "VeryLazy",
	},
	-- jump between letters with improved fFtT quicksearch, mimics sneak
	{ "ggandor/lightspeed.nvim", event = "VeryLazy" },

	-- yank from *anywhere* (even ssh session) to clipboard, using :OSCYank
	{ "ojroques/vim-oscyank", event = "VeryLazy" },
	-- personal dict improvements for git sync
	{ "micarmst/vim-spellsync", event = "VeryLazy" },
	{
		"folke/which-key.nvim",
		config = true,
		event = "VeryLazy",
	},
	-- collection of plugins
	{
		"echasnovski/mini.nvim",
		version = "*",
		config = function()
			require("plugins.config.mini")
		end,
		event = "VimEnter", -- need to load pretty soon for Starter screen
	},
	-- simpler, programmable and multiple terminal toggling for nvim
	{
		"akinsho/nvim-toggleterm.lua",
		config = function()
			require("plugins.config.toggleterm")
		end,
		lazy = false,
		cmd = { "ToggleTerm", "TermExec", "Lazygit", "Pythonterm" },
	},
	-- colorschemes
	{
		"RRethy/nvim-base16",
		lazy = false,
		priority = 1000,
		dependencies = { "rktjmp/fwatch.nvim" },
	},
}
