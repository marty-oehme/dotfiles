local util = require("lspconfig/util")
local path = util.path
local T = {}
local exepath = vim.fn.exepath

local path_sep = function()
	local is_win = vim.loop.os_uname().sysname:find("Windows")
	if is_win then
		return "\\"
	else
		return "/"
	end
end

-- from https://github.com/ray-x/navigator.lua/issues/247#issue-1465308677
T.get_path = function(workspace)
	-- Use activated virtualenv.
	if vim.env.VIRTUAL_ENV then
		return path.join(vim.env.VIRTUAL_ENV, "bin", "python"), "virtual env"
	end

	-- Find and use virtualenv in workspace directory.
	for _, pattern in ipairs({ "*", ".*" }) do
		local match = vim.fn.glob(path.join(workspace, pattern, "pyvenv.cfg"))
		local py = "bin" .. path_sep() .. "python"
		if match ~= "" then
			match = string.gsub(match, "pyvenv.cfg", py)
			return match, string.format("venv base folder: %s", match)
		end
		match = vim.fn.glob(path.join(workspace, pattern, "poetry.lock"))
		if match ~= "" then
			local venv_base_folder = vim.fn.trim(vim.fn.system("poetry env info -p"))
			return path.join(venv_base_folder, "bin", "python"), string.format("venv base folder: %s", venv_base_folder)
		end
	end

	-- Fallback to system Python.
	return exepath("python3") or exepath("python") or "python", "fallback to system python path"
end

return T
