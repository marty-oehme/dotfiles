--[[
creates a 'scratch' buffer which is ephemeral --
it will disappear when vim closes, does not save to anything and does not
appear in the buffer list. Useful for e.g. jotting down quick notes and thoughts.

If called with bang, will replace the current buffer with the scratch
window, otherwise opens a new split.

The buffer, by default is set to the pandoc filetype.
This can be changed by setting the `g:scratchpad_ft` variable or the `b:scratchpad_ft`
variable to the intended filetype.
]]
--
local api = vim.api
local M = {}

local function isempty(s)
	return s == nil or s == ""
end

function M.create(split, ft)
	-- should we create a new split or switch out the current buffer?
	if isempty(split) then
		split = false
	else
		split = true
	end
	-- which filetype to set for the scratchpad, defaults to pandoc
	if isempty(ft) then
		ft = vim.b["scratchpad_ft"] or vim.g["scratchpad_ft"] or "pandoc"
	end

	local buf = api.nvim_create_buf(false, true)
	if buf == 0 then
		print("Error opening scratch buffer.")
	end
	api.nvim_buf_set_option(buf, "bufhidden", "hide")
	api.nvim_buf_set_option(buf, "buftype", "nofile")
	api.nvim_buf_set_option(buf, "swapfile", false)
	api.nvim_buf_set_option(buf, "filetype", ft)

	if split then
		api.nvim_command("vsplit new")
	end -- i think this is the only way to interact with the buffers creating a new split
	-- switch to scratchpad
	api.nvim_win_set_buf(0, buf)
end

return M
