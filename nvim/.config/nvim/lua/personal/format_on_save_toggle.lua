local function stop_formatting()
	vim.api.nvim_del_augroup_by_name("LspFormat")
end

local function start_formatting()
	for _, client in pairs(vim.lsp.get_active_clients()) do
		require("lsp-setup.utils").format_on_save(client)
	end
end

local function _toggle(opts)
	if opts.args == "" then
		vim.g.format_on_save = not vim.g.format_on_save
	elseif opts.args == "on" or opts.args == "1" then
		vim.g.format_on_save = true
	elseif opts.args == "off" or opts.args == "0" then
		vim.g.format_on_save = false
	else
		vim.notify("Please provide arguments 'on' or 'off' or non arguments to toggle.")
	end

	if vim.g.format_on_save == true then
		start_formatting()
	else
		stop_formatting()
	end
end

vim.api.nvim_create_user_command(
	"FormatOnSave",
	_toggle,
	{ desc = "toggle automatically formatting on save", nargs = "?" }
)
