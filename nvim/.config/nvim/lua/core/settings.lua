local default_builtins_disabled = { "netrw", "netrwPlugin" }
local disable_builtins = function(builtins)
	for _, plugin in pairs(builtins) do
		vim.g["loaded_" .. plugin] = 1
	end
end
disable_builtins(default_builtins_disabled)

if vim.fn.has("nvim-0.9") == 1 then
	vim.opt.diffopt:append("linematch:60")
end

local options = {
	termguicolors = true,
	-- sets tabs to be 2 characters, expanded into spaces, but still removable with
	-- one press of backspace.
	-- great explanation: http://vimcasts.org/transcripts/2/en/
	tabstop = 4,
	shiftwidth = 4,
	softtabstop = 4,
	expandtab = true,
	-- make jumplist behave more like browser, when jumping back
	-- and then adding a new jump discards all the previous
	-- 'future-jump' tree, making more sense for wiki-like movement
	jumpoptions = "stack",
	-- set cursor line highlighting, esp useful when using with bracket
	-- highlighting and you don't know which side of the brace the cursor is on
	cursorline = true,
	-- shows linenumbers relative to the one you are on, for easy movement and
	-- dNUMBERd deletions
	number = true,
	relativenumber = true,
	-- puts the numbers into the signcolumn so when git/lsp show signs there's no jump
	signcolumn = "number",
	-- keeps an undofile next to files so that you can even undo if vim is closed
	-- in between
	undofile = true,
	-- TODO o.undodir = '~/.cache/nvim/undodir'

	-- ignores case by default but will use case when search is specifically not
	-- all lowercased
	ignorecase = true,
	smartcase = true,
	-- shows previews of what substitute command will do (and a couple others)
	inccommand = "split",
	-- disables showing us the current mode in the command line since airline takes
	-- care of it
	showmode = false,
	-- turn off modeline, to ensure security observation
	modeline = false,
	-- i feel foldlevel 2 is generally pretty usable, for headlines and similar
	-- set to use treesitter in treesitter config
	foldlevel = 2,
	conceallevel = 2,
	foldcolumn = vim.fn.has("nvim-0.9") == 1 and "auto:1" or nil,
	-- enable mouse, doesn't bug me and might come in useful at some point
	mouse = "a",
	-- pump all clippings into the system clipboard
	clipboard = "unnamedplus",
	--  turn of automatic resizing of individual splits
	equalalways = false,
	--  make sure there's always *some* context below cursor
	scrolloff = 4,
	-- open new buffers bottomright
	splitright = true,
	splitbelow = true,
	-- remove command line if no command is currently present
	cmdheight = 0,
	-- try to cleverly manage indents by preserving them from line to line
	breakindent = true,
	copyindent = true,
	preserveindent = true,
	smartindent = true,
}

for o, v in pairs(options) do
	vim.opt[o] = v
end
vim.opt.shortmess:append({ s = true, I = true })
vim.opt.backspace:append({ "nostop" })

local globals = {
	mapleader = " ",
	maplocalleader = ",",
	tex_flavor = "latex",

	format_on_save = true, -- from personal toggle function
}

for o, v in pairs(globals) do
	vim.g[o] = v
end
