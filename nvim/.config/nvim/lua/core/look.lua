local colorsfile = vim.fn.stdpath("state") .. "/colorscheme.lua"
local function source_colors()
	if vim.fn.filereadable(colorsfile) == 1 then
		vim.cmd("source " .. colorsfile)
	end
end

-- set on startup
source_colors()

-- continuously watch colors file for changes
local fwatch = require("fwatch")
fwatch.watch(colorsfile, {
	on_event = vim.schedule_wrap(function()
		source_colors()
	end),
})
