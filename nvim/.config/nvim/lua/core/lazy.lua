local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
-- inspired from astronvim lazy bootstrapping
if not vim.loop.fs_stat(lazypath) then
	local output = vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
	if vim.api.nvim_get_vvar("shell_error") ~= 0 then
		vim.api.nvim_err_writeln("Error cloning lazy.nvim repository...\n\n" .. output)
	end
	local oldcmdheight = vim.opt.cmdheight:get()
	vim.opt.cmdheight = 1
	vim.notify("Please wait while plugins are installed...")
	vim.api.nvim_create_autocmd("User", {
		desc = "Load Mason and Treesitter after Lazy installs plugins",
		once = true,
		pattern = "LazyInstall",
		callback = function()
			vim.cmd.bw()
			vim.opt.cmdheight = oldcmdheight
			vim.tbl_map(function(module)
				pcall(require, module)
			end, { "nvim-treesitter", "mason" })
			vim.notify("Mason is installing packages if configured, check status with :Mason")
		end,
	})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	spec = { { import = "plugins" } },
	defaults = { lazy = true, version = "*" },
	performance = { rtp = { disabled_plugins = { "netrw", "netrwPlugin" } } },
})
