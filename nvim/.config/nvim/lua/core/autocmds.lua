-- Highlight whatever is being yanked
vim.api.nvim_create_autocmd({ "TextYankPost" }, {
    command = 'silent! lua require"vim.highlight".on_yank{timeout=500}',
    desc = "Highlight yanked text whenevery yanking something",
    group = vim.api.nvim_create_augroup("highlightyanks", { clear = true }),
})

-- Special setting for editing gopass files - make sure nothing leaks outside the directories it is supposed to
vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
    pattern = {
        "/dev/shm/gopass.*",
        "/dev/shm/pass.?*/?*.txt",
        "$TMPDIR/pass.?*/?*.txt",
        "/tmp/pass.?*/?*.txt",
    },
    command = "setlocal noswapfile nobackup noundofile nowritebackup viminfo=",
    desc = "Don't leak any information when editing potential password files",
    group = vim.api.nvim_create_augroup("passnoleak", { clear = true }),
})

-- fixing neovim opening up at same moment as alacritty (see https://github.com/neovim/neovim/issues/11330)
vim.api.nvim_create_autocmd({ "VimEnter" }, {
    callback = function()
        local pid, WINCH = vim.fn.getpid(), vim.loop.constants.SIGWINCH
        vim.defer_fn(function()
            vim.loop.kill(pid, WINCH)
        end, 20)
    end,
    desc = "Fix neovim sizing issues if opening same time as alacritty",
    group = vim.api.nvim_create_augroup("alacritty_fixsize", { clear = true }),
})

-- remove line numbers from terminal buffers
vim.api.nvim_create_autocmd({ "TermOpen" }, {
    desc = "Hide buffer numbers for terminals",
    pattern = "*",
    command = "setlocal nonumber norelativenumber",
})
