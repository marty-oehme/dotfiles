#!/usr/bin/env sh
# Set up a non-english spell dictionary if it doesn't exist yet.

if [ ! -e "${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/spell/de.utf-8.spl" ]; then
    echo "Neovim spell dictionary not yet installed, downloading..."
    mkdir -p "${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/spell/"
    wget -q 'https://ftp.nluug.nl/pub/vim/runtime/spell/de.utf-8.spl' -O  "${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/spell/de.utf-8.spl"
    echo "Done."
fi


