#!/usr/bin/env python3

# From: https://gist.github.com/varunagrawal/2b93c5dc721520ff6876e940c420ab05
# This hooks script syncs task warrior to the configured task server.
# The on-exit event is triggered once, after all processing is complete.

# Make sure hooks are enabled and this hook script is executable.
# Run `task diag` for diagnostics on the hook.

import sys
import json
import subprocess

try:
    tasks = json.loads(sys.stdin.readline())
except:
    # No input
    sys.exit(0)

# no tasks to work through, don't error
if len(tasks) <= 0:
    sys.exit(0)

# Call the `sync` command
# hooks=0 ensures that the sync command doesn't call the on-exit hook
# verbose=nothing sets the verbosity to print nothing at all
subprocess.Popen(["task", "rc.verbose=nothing", "rc.hooks=0", "sync"], close_fds=True)

sys.exit(0)
