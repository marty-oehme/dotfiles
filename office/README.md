# office module

neomutt - terminal mail client
[mbsync](https://isync.sourceforge.io/mbsync.html) - mirror your Imapped mail directory locally with two-way synchronization
[notmuch](https://notmuchmail.org/) - index and search your mail
msmtp - send mail through shell commands
imapfilter - rule-based filtering engine for your incoming mail
vdirsyncer - mirror your contacts and calendars locally with two-way synchronization
khal - terminal vdir-compatible calendar client
calcurse - a pretty terminal calendar client, but not very vdir compatible
taskwarrior - a super flexible terminal to-do list

The module has three areas of concern: a functioning mail suite (receiving, sending, searching), to-do management, and contacts and calendaring.
Take care that most of the credential/account setup of course is highly specific to my setup and should be changed as required.

## mail

The current mail setup uses `mbsync` (from the isync project) to locally mirror the GMail imap folder.
It is run through a `sync-mail` script which can invoke pre- and post-sync hooks
(e.g. run your incoming mail through filters as soon as they arrive or ingest them into full-text search engines).[^1]
The sync script also automatically unlocks any pass directory so that credentials can be put into their respective applications and the directory closes again after use.
`msmtp` is used for mail sending by neomutt (and git if using git send-email).
`neomutt` then picks up the mail directory filled by `mbsync` and lets you browse it, respond to mail or create new ones.

[^1]: That being said, the script is a little dusty and could probably use a little overhaul. It *works* but could be more fantastic I feel.

## calendar and contacts

For calendars and contacts, the wonderful `vdirsyncer` is used to sync everything from a remote Dav client (hard-coded to my server currently) to the local directories in documents.
This is then picked up by `khal` to allow you to browse and edit your events, and create new ones - all of which in turn ends up remotely.

You can also browse your events locally with `calcurse` which will import them on each startup, but will not sync back yet;
meaning, any changes you do through the application will not reflect themselves back on any of your other connected devices unfortunately.

## tasks

Task management is done through the wonderful `taskwarrior`. I mostly use it as-is from the commandline, with some personal tweaks to urgencies and task dependencies.

The goal here is to integrate taskwarrior relatively tightly into mail
(e.g. receiving a mail and turning it into a task, then being able to go back from the task to the respective mail)
and calendaring (turning a to-do into a calendar event, and creating to-dos for individual events right from the calendar),
but those goals are still a ways off and the suite is working fine enough for now.
