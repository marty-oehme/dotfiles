function sendToFolder(folderFrom, folderTo, senders)
	local messages = folderFrom:select_all()
	for _, sender in pairs(senders) do
		local filtered = messages:contain_from(sender)
		filtered:mark_seen()
		filtered:move_messages(folderTo)
	end
end

-- will set filters to be grabbed from XDG-compliant filter directory
-- can be overridden with env var IMAPFILTER_ROLLUPFILE
function getRollupFile(fname)
	local f
	local fname = fname or "rollup.txt"
	if os.getenv("IMAPFILTER_ROLLUPFILE") then
		f = os.getenv("IMAPFILTER_ROLLUPFILE")
	elseif os.getenv("XDG_DATA_HOME") then
		f = os.getenv("XDG_DATA_HOME") .. "/imapfilter/" .. fname
	else
		f = os.getenv("HOME") .. "/.local/share/imapfilter/" .. fname
	end
	return f
end

function getSenderList(rollupfile)
	local rollupSenders = {}

	local file = io.open(rollupfile)
	if file then
		for line in file:lines() do
			table.insert(rollupSenders, line)
		end
	else
		print("rollup did not find rollup.txt file containing mail addresses at " .. rollupfile or ". Skipping.")
	end
	return rollupSenders
end

sendToFolder(accounts.gmail["Inbox"], accounts.gmail["Dump"], getSenderList(getRollupFile()))
