local accounts = {}

local _, gmailuser = pipe_from("pass show misc/gmail-app-password | grep username | cut -d: -f2")
local _, gmailpass = pipe_from("pass show misc/gmail-app-password | head -n1")
-- Setup an imap account called gmail
accounts.gmail = IMAP({
	server = "imap.gmail.com",
	username = gmailuser,
	password = gmailpass,
	ssl = "auto",
})

return accounts
