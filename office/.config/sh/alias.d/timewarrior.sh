#!/usr/bin/env sh

if ! exist timew; then
    return 1
fi

# invoking tim without arguments prints out timesheet summary
# with ids attached.
# otherwise tim passes through any arguments to timew
tim() {
    if [ "$#" -eq 0 ]; then
        timew summary :ids
    else
        timew "$@"
    fi
}
