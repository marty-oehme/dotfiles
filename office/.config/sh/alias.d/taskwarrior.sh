#!/usr/bin/env sh

# invoking t starts the task shell
# passing arguments along passes them straight through to taskwarrior instead
t() {
    # check for existence of tasksh before doing this whole song and dance
    if type tasksh >/dev/null 2>&1 && [ "$#" -eq 0 ]; then
        tasksh
    else
        task "$@"
    fi
}

# copy the `task` zsh completions over to my little alias 😉
# this is not very pretty and not super portable (needs ps) but
# works for now. from here: https://unix.stackexchange.com/a/72564/414758
if exist task && [ "$(ps -p $$ -o comm --no-headers)" = "zsh" ]; then compdef t=task; fi

# Make taskopen XDG conforming. If changing here, also change in taskrc alias.
alias taskopen='taskopen --config=${XDG_CONFIG_HOME:-~/.config}/task/taskopenrc'

alias ta="task add"
alias tal="task log"
alias tan="task annotate"

alias tn="task next +READY"
alias tun="task next urgency \> 4"
alias tod="task +TODAY or +OVERDUE"

if exist timew; then
    alias tra="task active && timew"
else
    alias tra="task active"
fi
alias trw="task end.after:today-1wk completed"

alias tad="task +ACTIVE done"
alias tas="task +ACTIVE stop"

alias to="task open"
