#!/usr/bin/env sh
#
# Wraps around the calcurse invocation and syncs calendar data
# to local vdir - given by default below.
#
# For now ONLY PROVIDES ONE-WAY Synchronization, see below.

# The path in which *the calendars* reside (i.e. toplevel with access to all paths)
CAL_PATH="$HOME/documents/calendars"

calcurse() {
    find "$CAL_PATH" -maxdepth 1 -type d -exec calcurse-vdir import {} \;
}

# Enable two-way sync. One issue is that calcurse would sync everything
# into the top-level path (or the selected calendar path) since it makes
# not the same differentiation as the vdir between calendars.
# FIXME Not sure how to resolve currently.
#
# The below works as a simple two-way synchronization on exiting calcurse.
# To function the invocation has to be turned from a function above to an
# executable shell-script file instead.
# trap 'calcurse_export' 0
#
# calcurse_export() {
#     calcurse-vdir export "$CAL_PATH"
# }
