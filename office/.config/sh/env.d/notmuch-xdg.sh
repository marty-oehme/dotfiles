#!/usr/bin/env sh
# Make notmuch config comply with xdg base dir specification

export NOTMUCH_CONFIG="${XDG_CONFIG_HOME:-~/.config}/notmuch/config"
