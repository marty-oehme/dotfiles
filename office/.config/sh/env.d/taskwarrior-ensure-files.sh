#!/usr/bin/env sh
# Ensure necessary taskwarrior includes exist

[ -e "${XDG_CONFIG_HOME:-$HOME/.config}/task/contexts" ] || touch "${XDG_CONFIG_HOME:-$HOME/.config}/task/contexts"
[ -e "${XDG_CONFIG_HOME:-$HOME/.config}/task/task-sync.rc" ] || touch "${XDG_CONFIG_HOME:-$HOME/.config}/task/task-sync.rc"
[ -e "${XDG_CONFIG_HOME:-$HOME/.config}/task/colorscheme" ] || touch "${XDG_CONFIG_HOME:-$HOME/.config}/task/colorscheme"
