#!/usr/bin/env sh
# Ensure the neomutt cache directories exist

[ -d "${XDG_CACHE_HOME:-$HOME/.cache}/neomutt" ] || mkdir -p "${XDG_CACHE_HOME:-$HOME/.cache}/neomutt/hcache"
