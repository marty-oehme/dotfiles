# index navigation
bind index l display-message
bind index L limit
bind index,pager g       noop # don't send group-replies with g
bind index       gg      first-entry
bind index       G       last-entry
bind index       <space> collapse-thread

# view manipulation
bind  attach,browser,index,pager \CF next-page
bind  attach,browser,index,pager \CB previous-page
bind  attach,browser,index,pager \Cu half-up
bind  attach,browser,index,pager \Cd half-down
bind  browser,index,pager        \Ce next-line
bind  browser,index,pager        \Cy previous-line

bind  index                      s   toggle-new
bind  index                      S   set-flag
macro index                      \CS "T~U<enter><tag-prefix><clear-flag>N<untag-pattern>.<enter>" "mark all messages as read"
bind  index,pager                w   display-toggle-weed

# Thread manipulation
bind pager d noop
bind index,pager    dD delete-message
bind index,pager    dd delete-subthread
bind index,pager    dt delete-thread
bind pager,index    gt next-thread
bind pager,index    gT previous-thread
bind pager,index    za collapse-thread
bind pager,index    zA collapse-all
bind pager,index    zr reconstruct-thread
bind pager,index    zR entire-thread
# Saner copy/move dialogs
macro index,pager   ,c "<copy-message>?<toggle-mailboxes>" "copy a message to a mailbox"
macro index,pager   ,m "<tag-thread><tag-prefix><save-message>?<toggle-mailboxes>" "move thread to a mailbox"
macro index,pager   ,M "<save-message>?<toggle-mailboxes>" "move a message to a mailbox"

# Email completion bindings
bind editor <Tab> complete-query
bind editor ^T complete
# add contact to Khard address book
macro index,pager ,a \
    "<pipe-message>khard add-email<return>" \
    "add the sender email address to khard"

# mailbox navigation
macro index,pager gi "<change-folder>=inbox<enter>" "go to inbox"
macro index,pager gm "<change-folder>=important<enter>" "go to important"
macro index,pager gs "<change-folder>=sent<enter>" "go to sent"
macro index,pager gd "<change-folder>=drafts<enter>" "go to drafts"
macro index,pager gt "<change-folder>=trash<enter>" "go to trash"
macro index,pager ga "<change-folder>=archive<enter>" "go to archive"
# sidebar navigation
bind index,pager B sidebar-toggle-visible
bind index,pager <down>   sidebar-next
bind index,pager <up>     sidebar-prev
bind index,pager <right>  sidebar-open
# search navigation
macro index a "<limit>all\n" "show all messages (undo limit)"
bind browser,index,pager N search-opposite # vim-like search
bind index \\ vfolder-from-query # notmuch search
# pager navigation
bind pager,attach h exit
bind pager l view-attachments
bind pager k  previous-line
bind pager j  next-line
bind pager gg top
bind pager G  bottom
# compose postpone
bind compose p postpone-message
# markdown to html for composition
macro compose ,m "F pandoc -s -f markdown -t html \ny^T^Utext/html; charset=UTF-8\n" "Convert from MD to HTML"

# since we unbound the original g
bind  index,pager r   noop              # to avoid accidentally sending replies
bind  index,pager rr  group-reply
bind  index,pager ro  reply
# open urls found in the e-mail
macro index,pager \CU "<enter-command> unset pipe_decode<enter><pipe-message>extract_url | fzf | clip<enter>" "get URLs"

# Refresh far imap email
macro index O "<sync-mailbox><shell-escape>export MBSYNC_PRE=true; sync-mail<enter><sync-mailbox>" "refresh all e-mail"
macro index o "<sync-mailbox><shell-escape>export MBSYNC_PRE=true; sync-mail gmail-inbox<enter><sync-mailbox>" "refresh inbox e-mail"


# Send mail to taskwarrior
macro index,pager ,T "<pipe-message>mutt2task -c -d -t<enter>" "add mail as task to taskwarrior with custom description and tags"
macro index,pager ,t "<pipe-message>mutt2task -c<enter>" "add mail as task to taskwarrior"
