# User services

This module uses `systemd` to provide various services for the user running the dotfiles.
The services will get moved into the correct `.config` directory for `systemd` to pick them up as user services.

They are, for now, *automatically* enabled when installing the dotfiles. This is pending changes.
They can be enabled or disabled as needed by invoking `systemctl --user enable|disable [service]`.
Be aware that, for the services to work correctly out of the box all the following applications have to exist on the system:
`an2linuxserver`, `dunst`, `greenclip`, `keybase`, `redshift`.
If they are not found, their service will fail silently (well, with a systemd error log, but who checks *those* first thing...)

Lastly, the directory `~/documents/notes/uni` will begin to be synced with `git`, so will need a) `git` installed, b) the directory to exist as a `git` directory, c) to be disabled if you do not want this directory to be continually synced.

Look into the sections below to see what else is assumed (e.g. a rigid notes and records directory structure).
Some of this may change over time -- ideally I would prefer flexible variables instead of hard-coded paths,
but it is what it is for now.
In other words:
you will **very likely** not just want to install and activate this module in general but be picky about what to do with it,
since it conforms to a highly opinionated set of installed applications and directory structures.

## Why systemd

There are two reasons for the service module making use of `systemd` to manage its services:

The machine(s) these dotfiles are currently targeting are both archlinux-based.
That means, pragmatically, I will adhere to using `systemd` since it is what arch uses as init manager.
I *am* eye-ing alternatives like [Artix Linux](https://artixlinux.org/) but adopting it is, if it happens at all, still a way out due to my current time constraints.

Secondly, I am using this opportunity to learn more about `systemd`.
There are various arcane-seeming invocations in `systemd` which are beginning to make more sense to me as time goes on
(looking at you, `journalctl`).
I do not want to dismiss a program without having given it a fair shake (especially if it enjoys *such* widespread adoption).
Additionally, process watching and automatic maintenance, as well as the entire `.timer` system that it can provide for services are pretty fantastic, even if their syntax takes some getting used to for me.

That said, there are also many moments where I am envious of the simplicity and straightforward nature of something like [runit](http://smarden.org/runit/).
So what that essentially means: this service module uses `systemd` *for now*.
It might change to something else in the future.
It might also not be written very well, since I am using it to adopt a mental mapping of `systemd` at the same time.

## Drop-in Changes to services

There are, for now, small changes to the services that get installed by default with their arch packages: `greenclip` and `redshift`.
Both need a running X server for their program to start successfully, and both are repeatedly run way too fast by systemd by default and would crash.
I have not yet created a new target solely for X having started
(and by default you can not access system-wide systemd targets from a user-wide service),
so in the meantime these changes simply make the programs wait longer before trying to restart.
This generally gives ample time for the services to start correctly by themselves.

## Commit Notes service

This service `uni-notes-sync.service` keeps my university note directory in sync with a remote origin.
The script behind it (`git-sync`) can be used more generally to keep any git directory in sync.

The idea behind the services is: commit asap, push when finished.
It basically runs on two different timers, every x seconds (120 by default, can be changed with `GS_TIME_TO_COMMIT` env var) it will commit whatever changes took place in the directory.
After x runs of the commit timer *without changes* (30 by default, can be changed with `GS_UNCHANGED_RUNS_TO_PUSH`) it will push the git index to its default remote.

In effect this means, using the default values, the service will commit any changes made every two minutes and when *no* changes were made for an hour (±2minutes), it will push it to remote.

The two cycles can be changed independently from each other via the environment variables mentioned above so that, for example, the repository can be pushed every single time a commit takes place, or commits can be checked for on faster or shorter intervals.
As another example, if you want to check for changes every 30 seconds but still push every hour, you would do `GS_TIME_TO_COMMIT=30` and `GS_UNCHANGED_RUNS_TO_PUSH=120`.

## Dropdown services

The `dropdown-terminal.service` is very simple, and always keeps a terminal window running.
The program is started with a `scratchpad` class, which is picked up by [`i3`](i3) and automatically hidden.
You can then show/hide the terminal as a floating overlay as you need, mimicking a floating terminal
(by default with `super + shift + return`, though this may change).
When you close the window in any way, systemd automatically restarts it in the background.

The `dropdown-todo.service` is similar but instead of an empty terminal window,
it starts up a `nvim` instance which hides most of its interface and shows a to-do list.
The list, by default, is situated in `~/documents/records/todo.md` and can be displayed with `super + t`.
