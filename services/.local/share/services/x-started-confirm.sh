#!/usr/bin/env bash
# from: https://superuser.com/questions/759759/writing-a-service-that-depends-on-xorg/1351489#1351489

COUNTER=0

while true; do
    # Check whether or not socket exists
    if [ -S /tmp/.X11-unix/X0 ]; then
        exit 0
    fi

    ((++COUNTER))

    if [ "$COUNTER" -gt 20 ]; then
        printf "X did not start in time\n"
        exit 1
    fi
    printf "X not yet started, counter at %s\n" "$COUNTER"
    sleep 1
done
