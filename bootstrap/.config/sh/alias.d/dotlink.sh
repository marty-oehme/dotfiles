#!/usr/bin/env sh
# relink all stowed files from anywhere
# grepping is to remove meaningless stderr lines until this bug is fixed:
# https://github.com/aspiers/stow/issues/65
#
# redirection is a neat way to filter stderr msgs by redirecting stderr
# to stdout in a subshell, grepping in it, and redirecting back to stderr:
# https://stackoverflow.com/a/15936384
#
# to customize this to your own needs, change the `push folder` to the
# location of your dotfiles (stow) repository
alias dotlink="pushd ~/.dotfiles;\
  stow -R */ 2> >(grep -v 'Absolute/relative mismatch between Stow dir' 1>&2) ;\
  popd"
