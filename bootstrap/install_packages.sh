#!/bin/bash
#
# Simple app bootstrapping script

#=== main function ============================================================
# NAME:         main
# DESCRIPTION:  Display usage information for this script.
# PARAMETERS:   see usage function
#==============================================================================
PKG_TSV_FILE=${PKG_TSV_FILE:-bootstrap/packages_stable.tsv}
packages_repo="${BOOTSTRAP_PACKAGES:-$(grep -e '	R	' "$PKG_TSV_FILE" | cut -f1 -d'	')}"
packages_aur="${BOOTSTRAP_PACKAGES_AUR:-$(grep -e '	A	' "$PKG_TSV_FILE" | cut -f1 -d'	')}"
packages_pipx="${BOOTSTRAP_PACKAGES_PIPX:-$(grep -e '	P	' "$PKG_TSV_FILE" | cut -f1,5 -d'	')}"

main() {
    local cmd=""
    local ret=0

    case "$1" in
    -v | --version)
        printf "Package bootstrap script.\n\n©Marty Oehme\n\nVersion: 0.3\n"
        ;;
    -h | --help)
        printf "Usage: install [-f|--force][-v|--version][-h|--help]\n\n-f     Do not ask for any confirmations but force update and installation.\n"
        ;;
    -f | --force)
        install true
        ;;
    *)
        install false
        ;;
    esac
    shift

    $cmd "$@"
    ret=$((ret + $?))
    exit $ret
}

install_paru() {
    # check for existing paru installation
    if type paru >/dev/null 2>&1; then
        echo "Existing paru installation found ..........................................."
        return
    fi

    # use tmp dir to make paru
    target=$(mktemp -d)
    git clone https://aur.archlinux.org/paru.git "$target"
    cd "$target" || exit
    makepkg -si
}

update_repos() {
    unattended="$1"
    if "$unattended"; then
        paru -Sqyy --noconfirm
    else
        paru -Syy
    fi
}

install_packages() {
    unattended="$1"
    if "$unattended"; then
        echo "$packages_repo" "$packages_aur" | paru -Squ --noconfirm --needed -
    else
        echo "$packages_repo" | paru -Squ --needed -
        echo "$packages_aur" | paru -S --needed -
    fi
}

install_pipx() {
    if type pipx >/dev/null 2>&1; then
        echo "Existing pipx installation found .........................................."
        return
    fi
    if "$unattended"; then
        paru -S --noconfirm python-pipx
    else
        paru -S python-pipx
    fi
}

install_pipx_pkgs() {
    while IFS= read -r line; do
        if [ -z "$line" ]; then return; fi
        prog=$(echo "$line" | cut -f1 -d'	')
        pipx install "$prog"

        injections=$(echo "$line" | cut -f2 -d'	')
        for inject_args in ${injections//,/ }; do
            pipx inject "$prog" "$inject_args"
        done
    done <<<"$packages_pipx"
}

install() {
    unattended=$1
    echo "Beginning package bootstrap ..............................................."
    echo "Installing paru ............................................................"
    install_paru
    echo "Installing apps ..........................................................."
    update_repos "$unattended"
    install_packages "$unattended"
    echo "Done ......................................................................"
    echo "Installing pipx ..........................................................."
    install_pipx
    echo "Installing pipx packages .................................................."
    install_pipx_pkgs
    echo "Done ......................................................................"
}

main "$@"
