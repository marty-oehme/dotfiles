# bootstrap module

The bootstrapping module mainly concerns the setup of the repository itself -- installation of packages, setting up basic options and maintenance scripts.

* installs general list of packages, listed [here](bootstrap/packages.tsv)
* if githooks are enabled (either through install script, or manually) will compare installed packages with those on the package list on each commit and warn user about differences
* contains a simple alias `dotlink` which allows quickly re-linking dotfiles when they have been changed. This is useful to invoke when files have been removed or added and need to be sym-linked by stow again (only works for `~/.dotfiles` dot directory)
* contains an `update_package_list.sh` script which I can use to quickly repopulate the list of explicitly installed packages, noting down their source (repositories or AUR) and retaining their target, if I set any (only works for `~/.dotfiles` dot directory)

